﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stream_Overlay
{
    public class Map
    {
        public string map_name { get; set; }
        public string title { get; set; }
    }

    public class Maps
    {
        public ObservableCollection<Map> maps { get; set; } = new ObservableCollection<Map>();
    }
}
