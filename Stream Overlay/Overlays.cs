﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stream_Overlay
{
    public class Overlay
    {
        public string title { get; set; }
        public string animation { get; set; }
        public string bettings { get; set; }
        public string grid { get; set; }
        public string ui { get; set; }
        public string preview { get; set; }
        public string static_preview { get; set; }
        public string thumbnail { get; set; }
        public string type { get; set; }
        public string map_frame { get; set; }
        public string map_title { get; set; }
    }

    public class Overlays
    {
        public List<Overlay> overlays { get; set; }
    }
}
