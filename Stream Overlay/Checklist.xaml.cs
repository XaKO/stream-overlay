﻿using Brackets;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Util.Store;
using Groups;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Media;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using WPFMediaKit.DirectShow.Controls;

namespace Stream_Overlay
{
    /// <summary>
    /// Логика взаимодействия для Checklist.xaml
    /// </summary>
    public partial class Checklist : Window
    {
        public string FormatPR(double PR)
        {
            int Rating = 0;
            try
            {
                Rating = (int)Math.Ceiling(PR);
            }
            catch
            {
                return "";
            }
            if (Rating < 3)
                return "Conscript";
            if (Rating > 2 && Rating < 8)
                return "Private";
            if (Rating > 7 && Rating < 11)
                return "Lance Corporal";
            if (Rating > 10 && Rating < 14)
                return "Corporal";
            if (Rating > 13 && Rating < 17)
                return "Sergeant";
            if (Rating > 16 && Rating < 20)
                return "Master Sergeant";
            if (Rating > 19 && Rating < 23)
                return "2nd Lieutenant";
            if (Rating > 22 && Rating < 26)
                return "1st Lieutenant";
            if (Rating > 25 && Rating < 29)
                return "Captain";
            if (Rating > 28 && Rating < 32)
                return "Major";
            if (Rating > 31 && Rating < 35)
                return "Lieutenant Colonel";
            if (Rating > 34 && Rating < 38)
                return "Colonel";
            if (Rating > 37 && Rating < 41)
                return "Brigadier";
            if (Rating > 40 && Rating < 44)
                return "Major General";
            if (Rating > 43 && Rating < 47)
                return "Lieutenant General";
            if (Rating > 46 && Rating < 50)
                return "General";
            if (Rating > 49)
                return "Field Marshal";


            return "Conscript";
        }

        async Task<string> HttpGetAsync(string URI)
        {
            try
            {
                HttpClient hc = new HttpClient();
                Task<System.IO.Stream> result = hc.GetStreamAsync(URI);

                System.IO.Stream vs = await result;
                using (StreamReader am = new StreamReader(vs, Encoding.UTF8))
                {
                    return await am.ReadToEndAsync();
                }
            }
            catch
            {
                return "error";
            }
        }

        async Task<Donations> GetDonations(string URL)
        {
            string json = await HttpGetAsync(URL);
            Donations res = new Donations();
            res.data = new List<Donation>();
            try
            {
                res = JsonConvert.DeserializeObject<Donations>(json);
            }
            catch
            {
            }
            return res;
        }
        SoundPlayer Click = new SoundPlayer(Application.GetResourceStream(new Uri("Elements/Click.wav", UriKind.Relative)).Stream);

        public List<BettingMatch> bettingMatches = new List<BettingMatch>();

        public Checklist()
        {
            InitializeComponent();
            Cursor = new Cursor(Application.GetResourceStream(new Uri("pack://application:,,,/Elements/Cursor.cur")).Stream);

        }

        private void Button_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Click.Play();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://eso-community.net/viewtopic.php?f=559&t=15040");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Show();
            this.Close();
        }



        public void setMatch(ref FrameworkElement frameworkElement, byte type, Groups.Match M, CurrentGroups G)
        {
            TextBlock cP1N;
            TextBlock cP2N;
            ImageBrush cP1F;
            ImageBrush cP2F;
            TextBlock cP1S;
            TextBlock cP2S;
            Border cP1;
            Border cP2;

            if (type == 0)
            {
                cP1N = frameworkElement.FindName("leftPlayer1Name") as TextBlock;
                cP2N = frameworkElement.FindName("leftPlayer2Name") as TextBlock;
                cP1F = frameworkElement.FindName("leftPlayer1Flag") as ImageBrush;
                cP2F = frameworkElement.FindName("leftPlayer2Flag") as ImageBrush;
                cP1S = frameworkElement.FindName("leftPlayer1Score") as TextBlock;
                cP2S = frameworkElement.FindName("leftPlayer2Score") as TextBlock;
                cP1 = frameworkElement.FindName("leftPlayer1") as Border;
                cP2 = frameworkElement.FindName("leftPlayer2") as Border;

            }
            else
            if (type == 1)
            {
                cP1N = frameworkElement.FindName("centerPlayer1Name") as TextBlock;
                cP2N = frameworkElement.FindName("centerPlayer2Name") as TextBlock;
                cP1F = frameworkElement.FindName("centerPlayer1Flag") as ImageBrush;
                cP2F = frameworkElement.FindName("centerPlayer2Flag") as ImageBrush;
                cP1S = frameworkElement.FindName("centerPlayer1Score") as TextBlock;
                cP2S = frameworkElement.FindName("centerPlayer2Score") as TextBlock;
                cP1 = frameworkElement.FindName("centerPlayer1") as Border;
                cP2 = frameworkElement.FindName("centerPlayer2") as Border;

            }
            else
            {
                cP1N = frameworkElement.FindName("rightPlayer1Name") as TextBlock;
                cP2N = frameworkElement.FindName("rightPlayer2Name") as TextBlock;
                cP1F = frameworkElement.FindName("rightPlayer1Flag") as ImageBrush;
                cP2F = frameworkElement.FindName("rightPlayer2Flag") as ImageBrush;
                cP1S = frameworkElement.FindName("rightPlayer1Score") as TextBlock;
                cP2S = frameworkElement.FindName("rightPlayer2Score") as TextBlock;
                cP1 = frameworkElement.FindName("rightPlayer1") as Border;
                cP2 = frameworkElement.FindName("rightPlayer2") as Border;
            }

            cP1N.Text = G.players.Single(p => p.id == M.players[0].player_id).name;
            cP2N.Text = G.players.Single(p => p.id == M.players[1].player_id).name;
            try
            {
                cP1F.ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/" + G.players.Single(p => p.id == M.players[0].player_id).flag_image));
            }
            catch
            {
                cP1F.ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/xy.png"));

            }
            try
            {
                cP2F.ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/" + G.players.Single(p => p.id == M.players[1].player_id).flag_image));
            }
            catch
            {
                cP2F.ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/xy.png"));
            }
            if (M.players[0].score == null)
            {
                cP1S.Text = "0";
                cP2S.Text = "0";
                cP1.Background = (Brush)new BrushConverter().ConvertFrom("#AA000000");
                cP2.Background = (Brush)new BrushConverter().ConvertFrom("#AA000000");
            }
            else
            {
                cP1S.Text = M.players[0].score.ToString();
                cP2S.Text = M.players[1].score.ToString();
                if (M.players[0].won == null)
                {
                    cP1.Background = (Brush)new BrushConverter().ConvertFrom("#AA000000");
                    cP2.Background = (Brush)new BrushConverter().ConvertFrom("#AA000000");
                }
                else
                if (M.players[0].won.Value)
                {
                    cP1.Background = (Brush)new BrushConverter().ConvertFrom("#AA257225");
                    cP2.Background = (Brush)new BrushConverter().ConvertFrom("#AA6E1616");
                }
                else
                if (M.players[1].won.Value)
                {
                    cP2.Background = (Brush)new BrushConverter().ConvertFrom("#AA257225");
                    cP1.Background = (Brush)new BrushConverter().ConvertFrom("#AA6E1616");
                }

            }
        }


        public void setMatch2(ref FrameworkElement frameworkElement, byte type, Brackets.Match M, CurrentBrackets B)
        {
            TextBlock cP1N;
            TextBlock cP2N;
            ImageBrush cP1F;
            ImageBrush cP2F;
            TextBlock cP1S;
            TextBlock cP2S;
            Border cP1;
            Border cP2;

            if (type == 0)
            {
                cP1N = frameworkElement.FindName("leftPlayer1Name") as TextBlock;
                cP2N = frameworkElement.FindName("leftPlayer2Name") as TextBlock;
                cP1F = frameworkElement.FindName("leftPlayer1Flag") as ImageBrush;
                cP2F = frameworkElement.FindName("leftPlayer2Flag") as ImageBrush;
                cP1S = frameworkElement.FindName("leftPlayer1Score") as TextBlock;
                cP2S = frameworkElement.FindName("leftPlayer2Score") as TextBlock;
                cP1 = frameworkElement.FindName("leftPlayer1") as Border;
                cP2 = frameworkElement.FindName("leftPlayer2") as Border;

            }
            else
            if (type == 1)
            {
                cP1N = frameworkElement.FindName("centerPlayer1Name") as TextBlock;
                cP2N = frameworkElement.FindName("centerPlayer2Name") as TextBlock;
                cP1F = frameworkElement.FindName("centerPlayer1Flag") as ImageBrush;
                cP2F = frameworkElement.FindName("centerPlayer2Flag") as ImageBrush;
                cP1S = frameworkElement.FindName("centerPlayer1Score") as TextBlock;
                cP2S = frameworkElement.FindName("centerPlayer2Score") as TextBlock;
                cP1 = frameworkElement.FindName("centerPlayer1") as Border;
                cP2 = frameworkElement.FindName("centerPlayer2") as Border;

            }
            else
            {
                cP1N = frameworkElement.FindName("rightPlayer1Name") as TextBlock;
                cP2N = frameworkElement.FindName("rightPlayer2Name") as TextBlock;
                cP1F = frameworkElement.FindName("rightPlayer1Flag") as ImageBrush;
                cP2F = frameworkElement.FindName("rightPlayer2Flag") as ImageBrush;
                cP1S = frameworkElement.FindName("rightPlayer1Score") as TextBlock;
                cP2S = frameworkElement.FindName("rightPlayer2Score") as TextBlock;
                cP1 = frameworkElement.FindName("rightPlayer1") as Border;
                cP2 = frameworkElement.FindName("rightPlayer2") as Border;
            }

            cP1N.Text = B.players.Single(p => p.id == M.players[0].player_id).name;
            cP2N.Text = B.players.Single(p => p.id == M.players[1].player_id).name;

            try
            {
                cP1F.ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/" + B.players.Single(p => p.id == M.players[0].player_id).flag_image));
            }
            catch
            {
                cP1F.ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/xy.png"));

            }
            try
            {
                cP2F.ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/" + B.players.Single(p => p.id == M.players[1].player_id).flag_image));
            }
            catch
            {
                cP2F.ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/xy.png"));
            }
            if (M.players[0].score == null)
            {
                cP1S.Text = "0";
                cP2S.Text = "0";
                cP1.Background = (Brush)new BrushConverter().ConvertFrom("#AA000000");
                cP2.Background = (Brush)new BrushConverter().ConvertFrom("#AA000000");
            }
            else
            {
                cP1S.Text = M.players[0].score.ToString();
                cP2S.Text = M.players[1].score.ToString();
                if (M.players[0].won == null)
                {
                    cP1.Background = (Brush)new BrushConverter().ConvertFrom("#AA000000");
                    cP2.Background = (Brush)new BrushConverter().ConvertFrom("#AA000000");
                }
                else
                if (M.players[0].won.Value)
                {
                    cP1.Background = (Brush)new BrushConverter().ConvertFrom("#AA257225");
                    cP2.Background = (Brush)new BrushConverter().ConvertFrom("#AA6E1616");
                }
                else
                if (M.players[1].won.Value)
                {
                    cP2.Background = (Brush)new BrushConverter().ConvertFrom("#AA257225");
                    cP1.Background = (Brush)new BrushConverter().ConvertFrom("#AA6E1616");
                }

            }
        }

        private async Task<FrameworkElement> setMatches(FrameworkElement frameworkElement, string BracketTitle, List<Groups.Match> M, string RoundTitle, CurrentGroups G)
        {
            if (M != null)
                if (M.Count > 2)
                {
                    DoubleAnimation center = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(1));
                    CircleEase ease1 = new CircleEase();
                    ease1.EasingMode = EasingMode.EaseInOut;
                    center.EasingFunction = ease1;

                    DoubleAnimation center2 = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(1.1));
                    center2.EasingFunction = ease1;
                    DoubleAnimation center3 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(1));
                    center3.EasingFunction = ease1;
                    center3.BeginTime = TimeSpan.FromSeconds(8);
                    DoubleAnimation center4 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(1));
                    center4.EasingFunction = ease1;
                    center4.BeginTime = TimeSpan.FromSeconds(8);


                    ObjectAnimationUsingKeyFrames center5 = new ObjectAnimationUsingKeyFrames();
                    center5.BeginTime = TimeSpan.FromSeconds(9);
                    DiscreteObjectKeyFrame keyFrame = new DiscreteObjectKeyFrame(Visibility.Hidden, TimeSpan.FromSeconds(0));
                    center5.KeyFrames.Add(keyFrame);

                    ObjectAnimationUsingKeyFrames center6 = new ObjectAnimationUsingKeyFrames();
                    center6.BeginTime = TimeSpan.FromSeconds(0);
                    DiscreteObjectKeyFrame keyFrame1 = new DiscreteObjectKeyFrame(Visibility.Visible, TimeSpan.FromSeconds(0));
                    center6.KeyFrames.Add(keyFrame1);



                    (frameworkElement.FindName("tbBrackets") as TextBlock).Text = BracketTitle;
                    (frameworkElement.FindName("tbMatchesRound") as TextBlock).Text = RoundTitle;
                    for (int i = 0; i < M.Count; i++)
                    {
                        {
                            if (i == 0)
                            {
                                setMatch(ref frameworkElement, 1, M[i], G);
                                setMatch(ref frameworkElement, 0, M[M.Count - 1], G);
                                setMatch(ref frameworkElement, 2, M[i + 1], G);

                            }
                            else
                            if (i == M.Count - 1)
                            {
                                setMatch(ref frameworkElement, 1, M[i], G);
                                setMatch(ref frameworkElement, 0, M[i - 1], G);
                                setMatch(ref frameworkElement, 2, M[0], G);
                            }
                            else
                            {
                                setMatch(ref frameworkElement, 1, M[i], G);
                                setMatch(ref frameworkElement, 0, M[i - 1], G);
                                setMatch(ref frameworkElement, 2, M[i + 1], G);
                            }
                            (frameworkElement.FindName("ReflectedVisual") as Grid).BeginAnimation(Grid.VisibilityProperty, center6);
                            (frameworkElement.FindName("TransparentStopCenter") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center, HandoffBehavior.Compose);
                            (frameworkElement.FindName("BlackStopCenter") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center2, HandoffBehavior.Compose);
                            (frameworkElement.FindName("TransparentStopCenter") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center3, HandoffBehavior.Compose);
                            (frameworkElement.FindName("BlackStopCenter") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center4, HandoffBehavior.Compose);
                            (frameworkElement.FindName("ReflectedVisual") as Grid).BeginAnimation(Grid.VisibilityProperty, center5, HandoffBehavior.Compose);

                            (frameworkElement.FindName("ReflectedVisualLeft") as Grid).BeginAnimation(Grid.VisibilityProperty, center6);
                            (frameworkElement.FindName("TransparentStopLeft") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center);
                            (frameworkElement.FindName("BlackStopLeft") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center2);
                            (frameworkElement.FindName("TransparentStopLeft") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center3, HandoffBehavior.Compose);
                            (frameworkElement.FindName("BlackStopLeft") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center4, HandoffBehavior.Compose);
                            (frameworkElement.FindName("ReflectedVisualLeft") as Grid).BeginAnimation(Grid.VisibilityProperty, center5, HandoffBehavior.Compose);

                            (frameworkElement.FindName("ReflectedVisualRight") as Grid).BeginAnimation(Grid.VisibilityProperty, center6);
                            (frameworkElement.FindName("TransparentStopRight") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center);
                            (frameworkElement.FindName("BlackStopRight") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center2);
                            (frameworkElement.FindName("TransparentStopRight") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center3, HandoffBehavior.Compose);
                            (frameworkElement.FindName("BlackStopRight") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center4, HandoffBehavior.Compose);
                            (frameworkElement.FindName("ReflectedVisualRight") as Grid).BeginAnimation(Grid.VisibilityProperty, center5, HandoffBehavior.Compose);

                            await Task.Delay(10000);
                        }


                    }
                }
            return frameworkElement;

        }

        private async Task<FrameworkElement> setMatches2(FrameworkElement frameworkElement, string BracketTitle, List<Brackets.Match> M, string RoundTitle, CurrentBrackets B)
        {

            if (M != null)
                if (M.Count > 2)
                {
                    DoubleAnimation center = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(1));
                    CircleEase ease1 = new CircleEase();
                    ease1.EasingMode = EasingMode.EaseInOut;
                    center.EasingFunction = ease1;

                    DoubleAnimation center2 = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(1.1));
                    center2.EasingFunction = ease1;
                    DoubleAnimation center3 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(1));
                    center3.EasingFunction = ease1;
                    center3.BeginTime = TimeSpan.FromSeconds(8);
                    DoubleAnimation center4 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(1));
                    center4.EasingFunction = ease1;
                    center4.BeginTime = TimeSpan.FromSeconds(8);

                    ObjectAnimationUsingKeyFrames center5 = new ObjectAnimationUsingKeyFrames();
                    center5.BeginTime = TimeSpan.FromSeconds(9);
                    DiscreteObjectKeyFrame keyFrame = new DiscreteObjectKeyFrame(Visibility.Hidden, TimeSpan.FromSeconds(0));
                    center5.KeyFrames.Add(keyFrame);

                    ObjectAnimationUsingKeyFrames center6 = new ObjectAnimationUsingKeyFrames();
                    center6.BeginTime = TimeSpan.FromSeconds(0);
                    DiscreteObjectKeyFrame keyFrame1 = new DiscreteObjectKeyFrame(Visibility.Visible, TimeSpan.FromSeconds(0));
                    center6.KeyFrames.Add(keyFrame1);

                    (frameworkElement.FindName("tbBrackets") as TextBlock).Text = BracketTitle;
                    (frameworkElement.FindName("tbMatchesRound") as TextBlock).Text = RoundTitle;
                    for (int i = 0; i < M.Count; i++)
                    {
                        {
                            if (i == 0)
                            {
                                setMatch2(ref frameworkElement, 1, M[i], B);
                                setMatch2(ref frameworkElement, 0, M[M.Count - 1], B);
                                setMatch2(ref frameworkElement, 2, M[i + 1], B);

                            }
                            else
                            if (i == M.Count - 1)
                            {
                                setMatch2(ref frameworkElement, 1, M[i], B);
                                setMatch2(ref frameworkElement, 0, M[i - 1], B);
                                setMatch2(ref frameworkElement, 2, M[0], B);
                            }
                            else
                            {
                                setMatch2(ref frameworkElement, 1, M[i], B);
                                setMatch2(ref frameworkElement, 0, M[i - 1], B);
                                setMatch2(ref frameworkElement, 2, M[i + 1], B);
                            }
                            (frameworkElement.FindName("ReflectedVisual") as Grid).BeginAnimation(Grid.VisibilityProperty, center6);
                            (frameworkElement.FindName("TransparentStopCenter") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center, HandoffBehavior.Compose);
                            (frameworkElement.FindName("BlackStopCenter") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center2, HandoffBehavior.Compose);
                            (frameworkElement.FindName("TransparentStopCenter") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center3, HandoffBehavior.Compose);
                            (frameworkElement.FindName("BlackStopCenter") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center4, HandoffBehavior.Compose);
                            (frameworkElement.FindName("ReflectedVisual") as Grid).BeginAnimation(Grid.VisibilityProperty, center5, HandoffBehavior.Compose);

                            (frameworkElement.FindName("ReflectedVisualLeft") as Grid).BeginAnimation(Grid.VisibilityProperty, center6);
                            (frameworkElement.FindName("TransparentStopLeft") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center);
                            (frameworkElement.FindName("BlackStopLeft") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center2);
                            (frameworkElement.FindName("TransparentStopLeft") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center3, HandoffBehavior.Compose);
                            (frameworkElement.FindName("BlackStopLeft") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center4, HandoffBehavior.Compose);
                            (frameworkElement.FindName("ReflectedVisualLeft") as Grid).BeginAnimation(Grid.VisibilityProperty, center5, HandoffBehavior.Compose);

                            (frameworkElement.FindName("ReflectedVisualRight") as Grid).BeginAnimation(Grid.VisibilityProperty, center6);
                            (frameworkElement.FindName("TransparentStopRight") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center);
                            (frameworkElement.FindName("BlackStopRight") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center2);
                            (frameworkElement.FindName("TransparentStopRight") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center3, HandoffBehavior.Compose);
                            (frameworkElement.FindName("BlackStopRight") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center4, HandoffBehavior.Compose);
                            (frameworkElement.FindName("ReflectedVisualRight") as Grid).BeginAnimation(Grid.VisibilityProperty, center5, HandoffBehavior.Compose);

                            await Task.Delay(10000);
                        }
                    }
                }
            return frameworkElement;
        }


        private int Order = 0;


        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

            if ((Application.Current.MainWindow as SettingsDialog).OverlayList.SelectedItem == null)
                return;
            Overlay Name = (((Application.Current.MainWindow as SettingsDialog).OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay);
            MainWindow mainWindow = new MainWindow();
            using (FileStream fs = new FileStream(Path.Combine(Environment.CurrentDirectory, Name.grid), FileMode.Open))
            {
                FrameworkElement frameworkElement = XamlReader.Load(fs) as FrameworkElement;
                frameworkElement.BeginInit();
                frameworkElement.DataContext = mainWindow;
                mainWindow.Overlay.Content = frameworkElement;

                (frameworkElement.FindName("OverlayGrid") as Grid).Width = SystemParameters.PrimaryScreenWidth;
                (frameworkElement.FindName("OverlayGrid") as Grid).Height = SystemParameters.PrimaryScreenHeight;
                if ((Application.Current.MainWindow as SettingsDialog).tbAnimation.IsChecked == true)
                {
                    (frameworkElement.FindName("PreviewImage") as Image).Source = new BitmapImage(new Uri(Path.Combine(Environment.CurrentDirectory, Name.preview)));
                    (frameworkElement.FindName("Animation") as MediaUriElement).Source = new Uri(Path.Combine(Environment.CurrentDirectory, Name.animation));
                }
                else
                {
                    (frameworkElement.FindName("PreviewImage") as Image).Source = new BitmapImage(new Uri(Path.Combine(Environment.CurrentDirectory, Name.static_preview)));
                }

                 (frameworkElement.FindName("OverlayImage") as Image).Source = new BitmapImage(new Uri(Path.Combine(Environment.CurrentDirectory, Name.ui)));
                if (frameworkElement.FindName("viewMatches") != null)
                {
                    mainWindow.AnimateViewMatches.Interval = new TimeSpan(0, 5, 0);
                    mainWindow.AnimateViewMatches.Tick += async (object s, EventArgs eventArgs) =>
                    {
                        mainWindow.AnimateViewMatches.Stop();
                        (frameworkElement.FindName("OverlayContent") as Grid).Visibility = Visibility.Hidden;
                        List<bool> rounds = new List<bool>();
                        rounds.Add((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem != null);
                        rounds.Add((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem != null);
                        rounds.Add((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem != null);
                        rounds.Add((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem != null);
                        if (rounds.Any(a => a == true))
                        {


                            while (!rounds[Order])
                            {
                                Order++;
                                if (Order == 4)
                                    Order = 0;
                            }

                            if (Order == 0)
                            {
                                if ((Application.Current.MainWindow as SettingsDialog).BracketGroupList1.Text.Contains("Groups Qualifiers") || (Application.Current.MainWindow as SettingsDialog).BracketGroupList1.Text.Contains("Groups Placements"))
                                {
                                    frameworkElement = await setMatches(frameworkElement, (Application.Current.MainWindow as SettingsDialog).BracketGroupList1.Text, ((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>,
                                    (Application.Current.MainWindow as SettingsDialog).RoundList1.Text, (Application.Current.MainWindow as SettingsDialog).currentGroups1);
                                }
                                else
                                {
                                    frameworkElement = await setMatches2(frameworkElement, (Application.Current.MainWindow as SettingsDialog).BracketGroupList1.Text, (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches,
                                    (Application.Current.MainWindow as SettingsDialog).RoundList1.Text, (Application.Current.MainWindow as SettingsDialog).currentBrackets1);

                                }
                            }
                            if (Order == 1)
                            {
                                if ((Application.Current.MainWindow as SettingsDialog).BracketGroupList2.Text.Contains("Groups Qualifiers") || (Application.Current.MainWindow as SettingsDialog).BracketGroupList2.Text.Contains("Groups Placements"))
                                {
                                    frameworkElement = await setMatches(frameworkElement, (Application.Current.MainWindow as SettingsDialog).BracketGroupList2.Text, ((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>,
                                    (Application.Current.MainWindow as SettingsDialog).RoundList2.Text, (Application.Current.MainWindow as SettingsDialog).currentGroups2);
                                }
                                else
                                {
                                    frameworkElement = await setMatches2(frameworkElement, (Application.Current.MainWindow as SettingsDialog).BracketGroupList2.Text, (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches,
                                    (Application.Current.MainWindow as SettingsDialog).RoundList2.Text, (Application.Current.MainWindow as SettingsDialog).currentBrackets2);

                                }
                            }
                            if (Order == 2)
                            {
                                if ((Application.Current.MainWindow as SettingsDialog).BracketGroupList3.Text.Contains("Groups Qualifiers") || (Application.Current.MainWindow as SettingsDialog).BracketGroupList3.Text.Contains("Groups Placements"))
                                {
                                    frameworkElement = await setMatches(frameworkElement, (Application.Current.MainWindow as SettingsDialog).BracketGroupList3.Text, ((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>,
                                    (Application.Current.MainWindow as SettingsDialog).RoundList3.Text, (Application.Current.MainWindow as SettingsDialog).currentGroups3);
                                }
                                else
                                {
                                    frameworkElement = await setMatches2(frameworkElement, (Application.Current.MainWindow as SettingsDialog).BracketGroupList3.Text, (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches,
                                    (Application.Current.MainWindow as SettingsDialog).RoundList3.Text, (Application.Current.MainWindow as SettingsDialog).currentBrackets3);

                                }
                            }
                            if (Order == 3)
                            {
                                if ((Application.Current.MainWindow as SettingsDialog).BracketGroupList4.Text.Contains("Groups Qualifiers") || (Application.Current.MainWindow as SettingsDialog).BracketGroupList4.Text.Contains("Groups Placements"))
                                {
                                    frameworkElement = await setMatches(frameworkElement, (Application.Current.MainWindow as SettingsDialog).BracketGroupList4.Text, ((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>,
                                    (Application.Current.MainWindow as SettingsDialog).RoundList4.Text, (Application.Current.MainWindow as SettingsDialog).currentGroups4);
                                }
                                else
                                {
                                    frameworkElement = await setMatches2(frameworkElement, (Application.Current.MainWindow as SettingsDialog).BracketGroupList4.Text, (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches,
                                    (Application.Current.MainWindow as SettingsDialog).RoundList4.Text, (Application.Current.MainWindow as SettingsDialog).currentBrackets4);

                                }
                            }
                            Order++;
                            if (Order == 4)
                                Order = 0;

                        }
                        (frameworkElement.FindName("OverlayContent") as Grid).Visibility = Visibility.Visible;
                        mainWindow.AnimateViewMatches.Start();
                    };
                    mainWindow.AnimateViewMatches.Start();

                }

                if (frameworkElement.FindName("lRecentDonations") != null)
                {
                    mainWindow.AnimateDonation.Interval = new TimeSpan(0, 0, 0);
                    mainWindow.AnimateTopDonation.Interval = new TimeSpan(0, 0, 0);
                    DoubleAnimation animation = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(1));
                    CircleEase easingFunction = new CircleEase();
                    easingFunction.EasingMode = EasingMode.EaseInOut;
                    animation.EasingFunction = easingFunction;

                    DoubleAnimation animation2 = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(1));
                    animation2.BeginTime = TimeSpan.FromSeconds(1.5);
                    animation2.EasingFunction = easingFunction;
                    ObjectAnimationUsingKeyFrames animation3 = new ObjectAnimationUsingKeyFrames();
                    animation3.BeginTime = TimeSpan.FromSeconds(1.5);
                    DiscreteObjectKeyFrame keyFrame = new DiscreteObjectKeyFrame("", TimeSpan.FromSeconds(0));
                    animation3.KeyFrames.Add(keyFrame);


                    mainWindow.AnimateDonation.Tick += async (object s, EventArgs eventArgs) =>
                    {
                        mainWindow.AnimateDonation.Stop();

                            List<Donation> donations = new List<Donation>(mainWindow.RecentDonations.data);
                            if (donations.Any())
                            {
                                foreach (Donation d in donations)
                                {
                                    (frameworkElement.FindName("lRecentDonations") as TextBlock).BeginAnimation(TextBlock.OpacityProperty, animation);
                                    animation3.KeyFrames[0].Value = d.name + " - $" + Math.Round(Convert.ToDouble(d.amount, CultureInfo.InvariantCulture), 2).ToString();
                                    (frameworkElement.FindName("lRecentDonations") as TextBlock).BeginAnimation(TextBlock.TextProperty, animation3, HandoffBehavior.Compose);
                                    (frameworkElement.FindName("lRecentDonations") as TextBlock).BeginAnimation(OpacityProperty, animation2, HandoffBehavior.Compose);
                                    await Task.Delay(10000);
                                }
                            }
                            else
                            {
                                (frameworkElement.FindName("lRecentDonations") as TextBlock).Text = "updating...";
                                await Task.Delay(1000);
                            }
                        mainWindow.AnimateDonation.Start();
                    };

                    mainWindow.AnimateTopDonation.Tick += async (object s, EventArgs eventArgs) =>
                    {
                        mainWindow.AnimateTopDonation.Stop();
                            List<string> donations = new List<string>(mainWindow.TopDonations);
                            if (donations.Any())
                            {
                                foreach (string d in donations)
                                {
                                    (frameworkElement.FindName("lTopDonors") as TextBlock).BeginAnimation(TextBlock.OpacityProperty, animation);
                                    animation3.KeyFrames[0].Value = d;
                                    (frameworkElement.FindName("lTopDonors") as TextBlock).BeginAnimation(TextBlock.TextProperty, animation3, HandoffBehavior.Compose);
                                    (frameworkElement.FindName("lTopDonors") as TextBlock).BeginAnimation(OpacityProperty, animation2, HandoffBehavior.Compose);
                                    await Task.Delay(10000);
                                }
                            }
                            else
                            {
                                (frameworkElement.FindName("lTopDonors") as TextBlock).Text = "updating...";
                                await Task.Delay(1000);
                            }
                        mainWindow.AnimateTopDonation.Start();
                    };

                    mainWindow.UpdateDonation.Interval = new TimeSpan(0, 0, 0);
                    mainWindow.UpdateTopDonation.Interval = new TimeSpan(0, 0, 0);
                    mainWindow.UpdateDonation.Tick += async (object s, EventArgs eventArgs) =>
                    {
                        mainWindow.UpdateDonation.Stop();
                        mainWindow.UpdateDonation.Interval = new TimeSpan(0, 1, 0);
                        Donations donations = await GetDonations("https://streamlabs.com/api/v1.0/donations?access_token=b7uTLYktzogX2vURzkr0g2jQ2rzmGSQSWXj07nry&verified=1&currency=USD&limit=10");
                        if (donations.data.Count() > 0)
                            mainWindow.RecentDonations = donations;
                        mainWindow.UpdateDonation.Start();
                    };

                    mainWindow.UpdateTopDonation.Tick += async (object s, EventArgs eventArgs) =>
                    {
                        mainWindow.UpdateTopDonation.Stop();
                        mainWindow.UpdateTopDonation.Interval = new TimeSpan(0, 5, 0);
                        Donations donations = await GetDonations("https://streamlabs.com/api/v1.0/donations?access_token=b7uTLYktzogX2vURzkr0g2jQ2rzmGSQSWXj07nry&verified=1&currency=USD&limit=100");

                        Dictionary<string, DInfo> Top = new Dictionary<string, DInfo>();
                        double OverallDonations = 0;
                        while (donations.data.Count != 0)
                        {
                            foreach (Donation d in donations.data)
                            {
                                if (Top.ContainsKey(d.email))
                                    Top[d.email].amount += Convert.ToDouble(d.amount, CultureInfo.InvariantCulture);
                                else
                                {
                                    bool equalName = false;
                                    foreach (KeyValuePair<string, DInfo> entry in Top)
                                    {
                                        if (entry.Value.name.ToLower() == d.name.ToLower())
                                        {
                                            entry.Value.amount += Convert.ToDouble(d.amount, CultureInfo.InvariantCulture);
                                            equalName = true;
                                            break;
                                        }
                                    }
                                    if (!equalName)
                                    {
                                        DInfo dInfo = new DInfo();
                                        dInfo.name = d.name;
                                        dInfo.amount = Convert.ToDouble(d.amount, CultureInfo.InvariantCulture);
                                        Top.Add(d.email, dInfo);
                                    }
                                }
                                OverallDonations += Convert.ToDouble(d.amount, CultureInfo.InvariantCulture);
                            }
                            donations = await GetDonations("https://streamlabs.com/api/v1.0/donations?access_token=b7uTLYktzogX2vURzkr0g2jQ2rzmGSQSWXj07nry&verified=1&currency=USD&before=" + donations.data.Last().donation_id.ToString());
                        }

                        var sortedDict = from entry in Top orderby entry.Value.amount descending select entry;
                        if (sortedDict.Count() > 0)
                            mainWindow.TopDonations = sortedDict.Select(x => x.Value.name + " - $" + Math.Round(Convert.ToDouble(x.Value.amount, CultureInfo.InvariantCulture), 2).ToString()).Take(10).ToList();
                        mainWindow.UpdateTopDonation.Start();
                    };

                    mainWindow.AnimateDonation.Start();
                    mainWindow.UpdateDonation.Start();
                    mainWindow.AnimateTopDonation.Start();
                    mainWindow.UpdateTopDonation.Start();
                }

                frameworkElement.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                frameworkElement.Arrange(new Rect(0, 0, frameworkElement.DesiredSize.Width, frameworkElement.DesiredSize.Height));

                (frameworkElement.FindName("OverlayGrid") as Grid).Width = (frameworkElement.FindName("OverlayImage") as Image).ActualWidth;
                (frameworkElement.FindName("OverlayGrid") as Grid).Height = (frameworkElement.FindName("OverlayImage") as Image).ActualHeight;

                if (frameworkElement.FindName("Schedule") != null)
                {
                    var myCur = Application.GetResourceStream(new Uri("pack://application:,,,/Elements/Cursor.cur")).Stream;
                    (frameworkElement.FindName("Schedule") as TextBox).PreviewKeyDown += mainWindow.TextBox_PreviewKeyDown;
                    (frameworkElement.FindName("Schedule") as TextBox).Cursor = new Cursor(myCur);
                    (frameworkElement.FindName("Schedule") as TextBox).Focus();
                }
                if (frameworkElement.FindName("tbPlayer1") != null)
                {
                    var myCur = Application.GetResourceStream(new Uri("pack://application:,,,/Elements/Cursor.cur")).Stream;
                    (frameworkElement.FindName("tbPlayer1") as TextBox).PreviewKeyDown += mainWindow.TextBox_PreviewKeyDown;
                    (frameworkElement.FindName("tbPlayer1") as TextBox).Cursor = new Cursor(myCur);
                    (frameworkElement.FindName("tbPlayer1") as TextBox).Focus();
                    mainWindow.InputBindings.Add(new InputBinding(mainWindow.Score1Up, new KeyGesture(Key.Left, ModifierKeys.Control)));
                    mainWindow.InputBindings.Add(new InputBinding(mainWindow.Score1Down, new KeyGesture(Key.Left, ModifierKeys.Control | ModifierKeys.Shift)));
                }
                if (frameworkElement.FindName("tbPlayer2") != null)
                {
                    var myCur = Application.GetResourceStream(new Uri("pack://application:,,,/Elements/Cursor.cur")).Stream;

                    (frameworkElement.FindName("tbPlayer2") as TextBox).PreviewKeyDown += mainWindow.TextBox_PreviewKeyDown;
                    (frameworkElement.FindName("tbPlayer2") as TextBox).Cursor = new Cursor(myCur);
                    mainWindow.InputBindings.Add(new InputBinding(mainWindow.Score2Up, new KeyGesture(Key.Right, ModifierKeys.Control)));
                    mainWindow.InputBindings.Add(new InputBinding(mainWindow.Score2Down, new KeyGesture(Key.Right, ModifierKeys.Control | ModifierKeys.Shift)));
                }

                if (frameworkElement.FindName("imBettingGrunge") != null)
                {
                    (frameworkElement.FindName("imBettingGrunge") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Elements/Grunge.png"));
                }

                if (frameworkElement.FindName("ibSecondChance") != null)
                {
                    (frameworkElement.FindName("ibSecondChance") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Elements/Grunge.png"));
                }

                List<DisplayingMatch> displayingMatches = new List<DisplayingMatch>();


                if (Name.type == "tournament")
                {
                    if ((Application.Current.MainWindow as SettingsDialog).MatchList1.Text != "")
                    {
                        if ((Application.Current.MainWindow as SettingsDialog).BracketGroupList1.Text.Contains("Groups Qualifiers") || (Application.Current.MainWindow as SettingsDialog).BracketGroupList1.Text.Contains("Groups Placements"))
                        {
                            Dictionary<string, string> Maps = new Dictionary<string, string>();
                            foreach (Groups.MapPool map in (Application.Current.MainWindow as SettingsDialog).currentGroups1.round.map_pool)
                            {
                                if (!Maps.ContainsKey((Application.Current.MainWindow as SettingsDialog).currentGroups1.maps[map.order].name))
                                    Maps.Add((Application.Current.MainWindow as SettingsDialog).currentGroups1.maps[map.order].name,
                                     (Application.Current.MainWindow as SettingsDialog).currentGroups1.maps[map.order].minimap_image.Replace("esoc_", ""));
                            }
                            string casters = "";
                            string time = "";
                            foreach (Groups.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type != "interview")
                                {
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        time = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        casters = "Casted by " + string.Join(" & ", schedule.casters);
                                }
                            }
                            displayingMatches.Add(new DisplayingMatch()
                            {
                                isInterview = false,
                                time = time,
                                casters = casters,
                                p1 = (Application.Current.MainWindow as SettingsDialog).currentGroups1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[0].player_id).name,
                                p2 = (Application.Current.MainWindow as SettingsDialog).currentGroups1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[1].player_id).name,
                                a1 = (Application.Current.MainWindow as SettingsDialog).currentGroups1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[0].player_id).avatar_image,
                                a2 = (Application.Current.MainWindow as SettingsDialog).currentGroups1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[1].player_id).avatar_image,
                                BO = "Best of " + (Application.Current.MainWindow as SettingsDialog).currentGroups1.round.best_of.ToString(),
                                round = (Application.Current.MainWindow as SettingsDialog).RoundList1.Text,
                                maps = Maps,
                                bracketTitle = (Application.Current.MainWindow as SettingsDialog).BracketGroupList1.Text,
                                rank1 = (Application.Current.MainWindow as SettingsDialog).currentGroups1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[0].player_id).power_rating,
                                rank2 = (Application.Current.MainWindow as SettingsDialog).currentGroups1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[1].player_id).power_rating,
                                f1 = (Application.Current.MainWindow as SettingsDialog).currentGroups1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[0].player_id).flag_image,
                                f2 = (Application.Current.MainWindow as SettingsDialog).currentGroups1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[1].player_id).flag_image
                            }
                            );
                            foreach (Groups.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type == "interview")
                                {
                                    string itime = "";
                                    string icasters = "";
                                    string ititle = "";
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        itime = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        icasters = "Casted by " + string.Join(" & ", schedule.casters);

                                    if (schedule.player_id != null)
                                        ititle = "With " + (Application.Current.MainWindow as SettingsDialog).currentGroups1.players.Single(s => s.id == schedule.player_id).name;
                                    displayingMatches.Add(new DisplayingMatch()
                                    {
                                        isInterview = true,
                                        time = itime,
                                        InterviewTitle = ititle,
                                        casters = icasters,
                                        maps = new Dictionary<string, string>(),
                                        p1 = "",
                                        p2 = "",
                                        bracketTitle = ""

                                    });
                                }
                            }
                        }
                        else
                        if ((Application.Current.MainWindow as SettingsDialog).BracketGroupList1.Text.Contains("Elimination Brackets"))
                        {
                            Dictionary<string, string> Maps = new Dictionary<string, string>();
                            foreach (Brackets.MapPool map in (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).map_pool)
                            {
                                if (!Maps.ContainsKey((Application.Current.MainWindow as SettingsDialog).currentBrackets1.maps[map.order].name))
                                    Maps.Add((Application.Current.MainWindow as SettingsDialog).currentBrackets1.maps[map.order].name,
                                     (Application.Current.MainWindow as SettingsDialog).currentBrackets1.maps[map.order].minimap_image.Replace("esoc_", ""));
                            }
                            string casters = "";
                            string time = "";
                            foreach (Brackets.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type != "interview")
                                {
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        time = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        casters = "Casted by " + string.Join(" & ", schedule.casters);
                                }
                            }

                            displayingMatches.Add(new DisplayingMatch()
                            {
                                isInterview = false,
                                time = time,
                                casters = casters,
                                p1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[0].player_id).name,
                                p2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[1].player_id).name,
                                a1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[0].player_id).avatar_image,
                                a2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[1].player_id).avatar_image,
                                BO = "Best of " + (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).best_of.ToString(),
                                round = (Application.Current.MainWindow as SettingsDialog).RoundList1.Text,
                                maps = Maps,
                                bracketTitle = (Application.Current.MainWindow as SettingsDialog).BracketGroupList1.Text,
                                rank1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[0].player_id).power_rating,
                                rank2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[1].player_id).power_rating,
                                f1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[0].player_id).flag_image,
                                f2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets1.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).players[1].player_id).flag_image

                            }
                        );
                            foreach (Brackets.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList1.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type == "interview")
                                {
                                    string itime = "";
                                    string icasters = "";
                                    string ititle = "";
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        itime = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        icasters = "Casted by " + string.Join(" & ", schedule.casters);

                                    if (schedule.player_id != null)
                                        ititle = "With " + (Application.Current.MainWindow as SettingsDialog).currentBrackets1.players.Single(s => s.id == schedule.player_id).name;
                                    displayingMatches.Add(new DisplayingMatch()
                                    {
                                        isInterview = true,
                                        InterviewTitle = ititle,
                                        time = itime,
                                        casters = icasters,
                                        maps = new Dictionary<string, string>(),
                                        p1 = "",
                                        p2 = "",
                                        bracketTitle = ""

                                    });
                                }
                            }
                        }
                    }

                    if ((Application.Current.MainWindow as SettingsDialog).MatchList2.Text != "")
                    {
                        if ((Application.Current.MainWindow as SettingsDialog).BracketGroupList2.Text.Contains("Groups Qualifiers") || (Application.Current.MainWindow as SettingsDialog).BracketGroupList2.Text.Contains("Groups Placements"))
                        {
                            Dictionary<string, string> Maps = new Dictionary<string, string>();
                            foreach (Groups.MapPool map in (Application.Current.MainWindow as SettingsDialog).currentGroups2.round.map_pool)
                            {
                                if (!Maps.ContainsKey((Application.Current.MainWindow as SettingsDialog).currentGroups2.maps[map.order].name))
                                    Maps.Add((Application.Current.MainWindow as SettingsDialog).currentGroups2.maps[map.order].name,
                                     (Application.Current.MainWindow as SettingsDialog).currentGroups2.maps[map.order].minimap_image.Replace("esoc_", ""));
                            }
                            string casters = "";
                            string time = "";
                            foreach (Groups.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type != "interview")
                                {
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        time = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        casters = "Casted by " + string.Join(" & ", schedule.casters);
                                }
                            }

                            displayingMatches.Add(new DisplayingMatch()
                            {
                                isInterview = false,
                                time = time,
                                casters = casters,
                                p1 = (Application.Current.MainWindow as SettingsDialog).currentGroups2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[0].player_id).name,
                                p2 = (Application.Current.MainWindow as SettingsDialog).currentGroups2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[1].player_id).name,
                                a1 = (Application.Current.MainWindow as SettingsDialog).currentGroups2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[0].player_id).avatar_image,
                                a2 = (Application.Current.MainWindow as SettingsDialog).currentGroups2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[1].player_id).avatar_image,
                                BO = "Best of " + (Application.Current.MainWindow as SettingsDialog).currentGroups2.round.best_of.ToString(),
                                round = (Application.Current.MainWindow as SettingsDialog).RoundList2.Text,
                                maps = Maps,
                                bracketTitle = (Application.Current.MainWindow as SettingsDialog).BracketGroupList2.Text,
                                rank1 = (Application.Current.MainWindow as SettingsDialog).currentGroups2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[0].player_id).power_rating,
                                rank2 = (Application.Current.MainWindow as SettingsDialog).currentGroups2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[1].player_id).power_rating,
                                f1 = (Application.Current.MainWindow as SettingsDialog).currentGroups2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[0].player_id).flag_image,
                                f2 = (Application.Current.MainWindow as SettingsDialog).currentGroups2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[1].player_id).flag_image


                            }
                            );
                            foreach (Groups.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type == "interview")
                                {
                                    string itime = "";
                                    string icasters = "";
                                    string ititle = "";
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        itime = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        icasters = "Casted by " + string.Join(" & ", schedule.casters);

                                    if (schedule.player_id != null)
                                        ititle = "With " + (Application.Current.MainWindow as SettingsDialog).currentGroups2.players.Single(s => s.id == schedule.player_id).name;
                                    displayingMatches.Add(new DisplayingMatch()
                                    {
                                        isInterview = true,
                                        InterviewTitle = ititle,
                                        time = itime,
                                        casters = icasters,
                                        maps = new Dictionary<string, string>(),
                                        p1 = "",
                                        p2 = "",
                                        bracketTitle = ""

                                    });
                                }
                            }
                        }
                        else
                        if ((Application.Current.MainWindow as SettingsDialog).BracketGroupList2.Text.Contains("Elimination Brackets"))
                        {
                            Dictionary<string, string> Maps = new Dictionary<string, string>();
                            foreach (Brackets.MapPool map in (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).map_pool)
                            {
                                if (!Maps.ContainsKey((Application.Current.MainWindow as SettingsDialog).currentBrackets2.maps[map.order].name))
                                    Maps.Add((Application.Current.MainWindow as SettingsDialog).currentBrackets2.maps[map.order].name,
                                     (Application.Current.MainWindow as SettingsDialog).currentBrackets2.maps[map.order].minimap_image.Replace("esoc_", ""));
                            }
                            string casters = "";
                            string time = "";
                            foreach (Brackets.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type != "interview")
                                {
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        time = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        casters = "Casted by " + string.Join(" & ", schedule.casters);
                                }
                            }

                            displayingMatches.Add(new DisplayingMatch()
                            {
                                isInterview = false,
                                time = time,
                                casters = casters,
                                p1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[0].player_id).name,
                                p2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[1].player_id).name,
                                a1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[0].player_id).avatar_image,
                                a2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[1].player_id).avatar_image,
                                BO = "Best of " + (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).best_of.ToString(),
                                round = (Application.Current.MainWindow as SettingsDialog).RoundList2.Text,
                                maps = Maps,
                                bracketTitle = (Application.Current.MainWindow as SettingsDialog).BracketGroupList2.Text,
                                rank1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[0].player_id).power_rating,
                                rank2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[1].player_id).power_rating,
                                f1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[0].player_id).flag_image,
                                f2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets2.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).players[1].player_id).flag_image

                            }
                        );
                            foreach (Brackets.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList2.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type == "interview")
                                {
                                    string itime = "";
                                    string icasters = "";
                                    string ititle = "";
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        itime = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        icasters = "Casted by " + string.Join(" & ", schedule.casters);

                                    if (schedule.player_id != null)
                                        ititle = "With " + (Application.Current.MainWindow as SettingsDialog).currentBrackets2.players.Single(s => s.id == schedule.player_id).name;
                                    displayingMatches.Add(new DisplayingMatch()
                                    {
                                        isInterview = true,
                                        InterviewTitle = ititle,
                                        time = itime,
                                        casters = icasters,
                                        maps = new Dictionary<string, string>(),
                                        p1 = "",
                                        p2 = "",
                                        bracketTitle = ""

                                    });
                                }
                            }
                        }
                    }

                    if ((Application.Current.MainWindow as SettingsDialog).MatchList3.Text != "")
                    {
                        if ((Application.Current.MainWindow as SettingsDialog).BracketGroupList3.Text.Contains("Groups Qualifiers") || (Application.Current.MainWindow as SettingsDialog).BracketGroupList3.Text.Contains("Groups Placements"))
                        {
                            Dictionary<string, string> Maps = new Dictionary<string, string>();
                            foreach (Groups.MapPool map in (Application.Current.MainWindow as SettingsDialog).currentGroups3.round.map_pool)
                            {
                                if (!Maps.ContainsKey((Application.Current.MainWindow as SettingsDialog).currentGroups3.maps[map.order].name))
                                    Maps.Add((Application.Current.MainWindow as SettingsDialog).currentGroups3.maps[map.order].name,
                                     (Application.Current.MainWindow as SettingsDialog).currentGroups3.maps[map.order].minimap_image.Replace("esoc_", ""));
                            }
                            string casters = "";
                            string time = "";
                            foreach (Groups.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type != "interview")
                                {
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        time = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        casters = "Casted by " + string.Join(" & ", schedule.casters);
                                }
                            }

                            displayingMatches.Add(new DisplayingMatch()
                            {
                                isInterview = false,
                                time = time,
                                casters = casters,
                                p1 = (Application.Current.MainWindow as SettingsDialog).currentGroups3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[0].player_id).name,
                                p2 = (Application.Current.MainWindow as SettingsDialog).currentGroups3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[1].player_id).name,
                                a1 = (Application.Current.MainWindow as SettingsDialog).currentGroups3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[0].player_id).avatar_image,
                                a2 = (Application.Current.MainWindow as SettingsDialog).currentGroups3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[1].player_id).avatar_image,
                                BO = "Best of " + (Application.Current.MainWindow as SettingsDialog).currentGroups3.round.best_of.ToString(),
                                round = (Application.Current.MainWindow as SettingsDialog).RoundList3.Text,
                                maps = Maps,
                                bracketTitle = (Application.Current.MainWindow as SettingsDialog).BracketGroupList3.Text,
                                rank1 = (Application.Current.MainWindow as SettingsDialog).currentGroups3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[0].player_id).power_rating,
                                rank2 = (Application.Current.MainWindow as SettingsDialog).currentGroups3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[1].player_id).power_rating,
                                f1 = (Application.Current.MainWindow as SettingsDialog).currentGroups3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[0].player_id).flag_image,
                                f2 = (Application.Current.MainWindow as SettingsDialog).currentGroups3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[1].player_id).flag_image


                            }
                            );
                            foreach (Groups.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type == "interview")
                                {
                                    string itime = "";
                                    string icasters = "";
                                    string ititle = "";
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        itime = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        icasters = "Casted by " + string.Join(" & ", schedule.casters);

                                    if (schedule.player_id != null)
                                        ititle = "With " + (Application.Current.MainWindow as SettingsDialog).currentGroups3.players.Single(s => s.id == schedule.player_id).name;
                                    displayingMatches.Add(new DisplayingMatch()
                                    {
                                        isInterview = true,
                                        InterviewTitle = ititle,
                                        time = itime,
                                        casters = icasters,
                                        maps = new Dictionary<string, string>(),
                                        p1 = "",
                                        p2 = "",
                                        bracketTitle = ""

                                    });
                                }
                            }
                        }
                        else
                        if ((Application.Current.MainWindow as SettingsDialog).BracketGroupList3.Text.Contains("Elimination Brackets"))
                        {
                            Dictionary<string, string> Maps = new Dictionary<string, string>();
                            foreach (Brackets.MapPool map in (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).map_pool)
                            {
                                if (!Maps.ContainsKey((Application.Current.MainWindow as SettingsDialog).currentBrackets3.maps[map.order].name))
                                    Maps.Add((Application.Current.MainWindow as SettingsDialog).currentBrackets3.maps[map.order].name,
                                     (Application.Current.MainWindow as SettingsDialog).currentBrackets3.maps[map.order].minimap_image.Replace("esoc_", ""));
                            }
                            string casters = "";
                            string time = "";
                            foreach (Brackets.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type != "interview")
                                {
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        time = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        casters = "Casted by " + string.Join(" & ", schedule.casters);
                                }
                            }

                            displayingMatches.Add(new DisplayingMatch()
                            {
                                isInterview = false,
                                time = time,
                                casters = casters,
                                p1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[0].player_id).name,
                                p2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[1].player_id).name,
                                a1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[0].player_id).avatar_image,
                                a2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[1].player_id).avatar_image,
                                BO = "Best of " + (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).best_of.ToString(),
                                round = (Application.Current.MainWindow as SettingsDialog).RoundList3.Text,
                                maps = Maps,
                                bracketTitle = (Application.Current.MainWindow as SettingsDialog).BracketGroupList3.Text,
                                rank1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[0].player_id).power_rating,
                                rank2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[1].player_id).power_rating,
                                f1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[0].player_id).flag_image,
                                f2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets3.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).players[1].player_id).flag_image

                            }
                        );
                            foreach (Brackets.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList3.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type == "interview")
                                {
                                    string itime = "";
                                    string icasters = "";
                                    string ititle = "";
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        itime = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        icasters = "Casted by " + string.Join(" & ", schedule.casters);

                                    if (schedule.player_id != null)
                                        ititle = "With " + (Application.Current.MainWindow as SettingsDialog).currentBrackets3.players.Single(s => s.id == schedule.player_id).name;
                                    displayingMatches.Add(new DisplayingMatch()
                                    {
                                        isInterview = true,
                                        InterviewTitle = ititle,
                                        time = itime,
                                        casters = icasters,
                                        maps = new Dictionary<string, string>(),
                                        p1 = "",
                                        p2 = "",
                                        bracketTitle = ""

                                    });
                                }
                            }
                        }
                    }

                    if ((Application.Current.MainWindow as SettingsDialog).MatchList4.Text != "")
                    {
                        if ((Application.Current.MainWindow as SettingsDialog).BracketGroupList4.Text.Contains("Groups Qualifiers") || (Application.Current.MainWindow as SettingsDialog).BracketGroupList4.Text.Contains("Groups Placements"))
                        {
                            Dictionary<string, string> Maps = new Dictionary<string, string>();
                            foreach (Groups.MapPool map in (Application.Current.MainWindow as SettingsDialog).currentGroups4.round.map_pool)
                            {
                                if (!Maps.ContainsKey((Application.Current.MainWindow as SettingsDialog).currentGroups4.maps[map.order].name))
                                    Maps.Add((Application.Current.MainWindow as SettingsDialog).currentGroups4.maps[map.order].name,
                                     (Application.Current.MainWindow as SettingsDialog).currentGroups4.maps[map.order].minimap_image.Replace("esoc_", ""));
                            }
                            string casters = "";
                            string time = "";
                            foreach (Groups.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type != "interview")
                                {
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        time = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        casters = "Casted by " + string.Join(" & ", schedule.casters);
                                }
                            }

                            displayingMatches.Add(new DisplayingMatch()
                            {
                                isInterview = false,
                                time = time,
                                casters = casters,
                                p1 = (Application.Current.MainWindow as SettingsDialog).currentGroups4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[0].player_id).name,
                                p2 = (Application.Current.MainWindow as SettingsDialog).currentGroups4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[1].player_id).name,
                                a1 = (Application.Current.MainWindow as SettingsDialog).currentGroups4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[0].player_id).avatar_image,
                                a2 = (Application.Current.MainWindow as SettingsDialog).currentGroups4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[1].player_id).avatar_image,
                                BO = "Best of " + (Application.Current.MainWindow as SettingsDialog).currentGroups4.round.best_of.ToString(),
                                round = (Application.Current.MainWindow as SettingsDialog).RoundList4.Text,
                                maps = Maps,
                                bracketTitle = (Application.Current.MainWindow as SettingsDialog).BracketGroupList4.Text,
                                rank1 = (Application.Current.MainWindow as SettingsDialog).currentGroups4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[0].player_id).power_rating,
                                rank2 = (Application.Current.MainWindow as SettingsDialog).currentGroups4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[1].player_id).power_rating,
                                f1 = (Application.Current.MainWindow as SettingsDialog).currentGroups4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[0].player_id).flag_image,
                                f2 = (Application.Current.MainWindow as SettingsDialog).currentGroups4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[1].player_id).flag_image,


                            }
                            );
                            foreach (Groups.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type == "interview")
                                {
                                    string itime = "";
                                    string icasters = "";
                                    string ititle = "";
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        itime = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        icasters = "Casted by " + string.Join(" & ", schedule.casters);

                                    if (schedule.player_id != null)
                                        ititle = "With " + (Application.Current.MainWindow as SettingsDialog).currentGroups4.players.Single(s => s.id == schedule.player_id).name;
                                    displayingMatches.Add(new DisplayingMatch()
                                    {
                                        isInterview = true,
                                        InterviewTitle = ititle,
                                        time = itime,
                                        casters = icasters,
                                        maps = new Dictionary<string, string>(),
                                        p1 = "",
                                        p2 = "",
                                        bracketTitle = ""

                                    });
                                }
                            }
                        }
                        else
                        if ((Application.Current.MainWindow as SettingsDialog).BracketGroupList4.Text.Contains("Elimination Brackets"))
                        {
                            Dictionary<string, string> Maps = new Dictionary<string, string>();
                            foreach (Brackets.MapPool map in (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).map_pool)
                            {
                                if (!Maps.ContainsKey((Application.Current.MainWindow as SettingsDialog).currentBrackets4.maps[map.order].name))
                                    Maps.Add((Application.Current.MainWindow as SettingsDialog).currentBrackets4.maps[map.order].name,
                                     (Application.Current.MainWindow as SettingsDialog).currentBrackets4.maps[map.order].minimap_image.Replace("esoc_", ""));
                            }
                            string casters = "";
                            string time = "";
                            foreach (Brackets.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type != "interview")
                                {
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        time = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        casters = "Casted by " + string.Join(" & ", schedule.casters);
                                }
                            }

                            displayingMatches.Add(new DisplayingMatch()
                            {
                                isInterview = false,
                                time = time,
                                casters = casters,
                                p1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[0].player_id).name,
                                p2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[1].player_id).name,
                                a1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[0].player_id).avatar_image,
                                a2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[1].player_id).avatar_image,
                                BO = "Best of " + (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).best_of.ToString(),
                                round = (Application.Current.MainWindow as SettingsDialog).RoundList4.Text,
                                maps = Maps,
                                bracketTitle = (Application.Current.MainWindow as SettingsDialog).BracketGroupList4.Text,
                                rank1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[0].player_id).power_rating,
                                rank2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[1].player_id).power_rating,
                                f1 = (Application.Current.MainWindow as SettingsDialog).currentBrackets4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[0].player_id).flag_image,
                                f2 = (Application.Current.MainWindow as SettingsDialog).currentBrackets4.players.Single(s => s.id == (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).players[1].player_id).flag_image

                            }
                        );
                            foreach (Brackets.Schedule schedule in (((Application.Current.MainWindow as SettingsDialog).RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)((Application.Current.MainWindow as SettingsDialog).MatchList4.SelectedItem as ComboBoxItem).Tag).schedule)
                            {
                                if (schedule.type == "interview")
                                {
                                    string itime = "";
                                    string icasters = "";
                                    string ititle = "";
                                    if (!string.IsNullOrEmpty(schedule.start_date))
                                        itime = Convert.ToDateTime(schedule.start_date).ToString("dddd, dd MMMM HH:mm 'GMT'", new CultureInfo("en-US"));
                                    if (schedule.casters.Count > 0)
                                        icasters = "Casted by " + string.Join(" & ", schedule.casters);

                                    if (schedule.player_id != null)
                                        ititle = "With " + (Application.Current.MainWindow as SettingsDialog).currentBrackets4.players.Single(s => s.id == schedule.player_id).name;
                                    displayingMatches.Add(new DisplayingMatch()
                                    {
                                        isInterview = true,
                                        InterviewTitle = ititle,
                                        time = itime,
                                        casters = icasters,
                                        maps = new Dictionary<string, string>(),
                                        p1 = "",
                                        p2 = "",
                                        bracketTitle = ""

                                    });
                                }
                            }
                        }
                    }
                    if (frameworkElement.FindName("gDisplayMatch") != null)
                    {

                        mainWindow.AnimateMatch.Interval = new TimeSpan(0, 0, 0);
                        mainWindow.AnimateMatch.Tick += async (object s, EventArgs eventArgs) =>
                        {
                            mainWindow.AnimateMatch.Stop();
                            if (displayingMatches.Count > 0)
                            {
                                int DMIndex = 1;
                                foreach (DisplayingMatch DM in displayingMatches)
                                {

                                    DoubleAnimation center = new DoubleAnimation(-350, 0, TimeSpan.FromSeconds(2));
                                    DoubleAnimation center2 = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(2));
                                    ElasticEase ease1 = new ElasticEase();
                                    ease1.EasingMode = EasingMode.EaseOut;
                                    ease1.Oscillations = 3;
                                    ease1.Springiness = 8;

                                    CircleEase ease2 = new CircleEase();
                                    ease2.EasingMode = EasingMode.EaseOut;
                                    center.EasingFunction = ease1;
                                    center2.EasingFunction = ease2;


                                    DoubleAnimation center3 = new DoubleAnimation(0, 300, TimeSpan.FromSeconds(1));
                                    DoubleAnimation center4 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(1));

                                    CircleEase ease4 = new CircleEase();
                                    ease4.EasingMode = EasingMode.EaseInOut;
                                    center3.EasingFunction = ease2;
                                    center4.EasingFunction = ease2;
                                    center3.BeginTime = TimeSpan.FromSeconds(6 * DM.maps.Count + 1.5);
                                    center4.BeginTime = TimeSpan.FromSeconds(6 * DM.maps.Count + 1.5);



                                    ObjectAnimationUsingKeyFrames center12 = new ObjectAnimationUsingKeyFrames();
                                    center12.BeginTime = TimeSpan.FromSeconds(1.5);
                                    DiscreteObjectKeyFrame keyFrame = new DiscreteObjectKeyFrame(Visibility.Visible, TimeSpan.FromSeconds(0));
                                    center12.KeyFrames.Add(keyFrame);

                                    ObjectAnimationUsingKeyFrames center13 = new ObjectAnimationUsingKeyFrames();
                                    center13.BeginTime = TimeSpan.FromSeconds(2.5);
                                    center13.KeyFrames.Add(keyFrame);



                                    ObjectAnimationUsingKeyFrames center14 = new ObjectAnimationUsingKeyFrames();
                                    center14.BeginTime = TimeSpan.FromSeconds(0);
                                    DiscreteObjectKeyFrame keyFrame2 = new DiscreteObjectKeyFrame(Visibility.Hidden, TimeSpan.FromSeconds(0));
                                    center14.KeyFrames.Add(keyFrame2);

                                    ObjectAnimationUsingKeyFrames center15 = new ObjectAnimationUsingKeyFrames();
                                    center15.BeginTime = TimeSpan.FromSeconds(0);
                                    center15.KeyFrames.Add(keyFrame);


                                    if (frameworkElement.FindName("gSecondChance") != null)
                                    {
                                        if (DM.bracketTitle.Contains("Second Chance"))
                                        {
                                            (frameworkElement.FindName("gSecondChance") as Grid).Visibility = Visibility.Visible;
                                            DoubleAnimation center17 = new DoubleAnimation(3, 1, TimeSpan.FromMilliseconds(500));
                                            DoubleAnimation center18 = new DoubleAnimation(1, 0, TimeSpan.FromMilliseconds(300));
                                            ElasticEase ease5 = new ElasticEase();
                                            ease5.EasingMode = EasingMode.EaseOut;
                                            ease5.Oscillations = 1;
                                            ease5.Springiness = 4;
                                            center17.BeginTime = TimeSpan.FromSeconds(0);
                                            center18.BeginTime = TimeSpan.FromSeconds(6 * DM.maps.Count + 1.5);
                                            center17.EasingFunction = ease5;
                                            center18.EasingFunction = ease2;
                                            if (displayingMatches.Count > 1)
                                            {
                                                (frameworkElement.FindName("stSecondChance") as ScaleTransform).BeginAnimation(ScaleTransform.ScaleXProperty, center17);
                                                (frameworkElement.FindName("stSecondChance") as ScaleTransform).BeginAnimation(ScaleTransform.ScaleYProperty, center17);
                                                (frameworkElement.FindName("stSecondChance") as ScaleTransform).BeginAnimation(ScaleTransform.ScaleXProperty, center18, HandoffBehavior.Compose);
                                                (frameworkElement.FindName("stSecondChance") as ScaleTransform).BeginAnimation(ScaleTransform.ScaleYProperty, center18, HandoffBehavior.Compose);
                                            }
                                        }

                                    }

                                    if (bettingMatches.Count > 0)
                                    {
                                        var bM = bettingMatches.SingleOrDefault(m => (m.p1 == DM.p1 && m.p2 == DM.p2) || (m.p2 == DM.p1 && m.p1 == DM.p2));
                                        if (bM != null)
                                        {
                                            if (bM.p2 == DM.p1 && bM.p1 == DM.p2)
                                            {
                                                var p = bM.p1;
                                                var o = bM.odds1;
                                                bM.p1 = bM.p2;
                                                bM.odds1 = bM.odds2;
                                                bM.p2 = p;
                                                bM.odds2 = o;
                                            }
                                            if (bM.odds1 == null)
                                            {
                                                (frameworkElement.FindName("tbBetsRatio1") as TextBlock).Text = "N/A";
                                                (frameworkElement.FindName("gsBets2Main1") as GradientStop).Color = (Color)new ColorConverter().ConvertFrom("#AA257225");
                                            }
                                            else
                                            {
                                                (frameworkElement.FindName("tbBetsRatio1") as TextBlock).Text = Math.Round(bM.odds1.Value * 100).ToString() + " %";
                                                if (bM.odds2 != null)
                                                    if (Math.Round(bM.odds1.Value * 100) > Math.Round(bM.odds2.Value * 100))
                                                    {
                                                        (frameworkElement.FindName("gsBets1Main1") as GradientStop).Color = (Color)new ColorConverter().ConvertFrom("#AA257225");
                                                        (frameworkElement.FindName("gsBets2Main1") as GradientStop).Color = (Color)new ColorConverter().ConvertFrom("#AA6E1616");
                                                    }
                                                    else
                                                    if (Math.Round(bM.odds1.Value * 100) < Math.Round(bM.odds2.Value * 100))
                                                    {
                                                        (frameworkElement.FindName("gsBets1Main1") as GradientStop).Color = (Color)new ColorConverter().ConvertFrom("#AA6E1616");
                                                        (frameworkElement.FindName("gsBets2Main1") as GradientStop).Color = (Color)new ColorConverter().ConvertFrom("#AA257225");
                                                    }
                                                    else
                                                    {
                                                        (frameworkElement.FindName("gsBets1Main1") as GradientStop).Color = (Color)new ColorConverter().ConvertFrom("#AA257225");
                                                        (frameworkElement.FindName("gsBets2Main1") as GradientStop).Color = (Color)new ColorConverter().ConvertFrom("#AA257225");
                                                    }

                                            }

                                            if (bM.odds2 == null)
                                                (frameworkElement.FindName("tbBetsRatio2") as TextBlock).Text = "N/A";
                                            else
                                                (frameworkElement.FindName("tbBetsRatio2") as TextBlock).Text = Math.Round(bM.odds2.Value * 100).ToString() + " %";
                                            DoubleAnimation center9 = new DoubleAnimation(5, 1, TimeSpan.FromSeconds(1));
                                            center9.EasingFunction = ease1;
                                            center9.BeginTime = TimeSpan.FromSeconds(1.5);
                                            DoubleAnimation center10 = new DoubleAnimation(0, bM.odds1 ?? 0, TimeSpan.FromSeconds(0.8));
                                            center10.EasingFunction = ease2;
                                            center10.BeginTime = TimeSpan.FromSeconds(2.5);
                                            DoubleAnimation center11 = new DoubleAnimation(0, bM.odds2 ?? 0, TimeSpan.FromSeconds(0.8));
                                            center11.EasingFunction = ease2;
                                            center11.BeginTime = TimeSpan.FromSeconds(2.5);


                                            if (displayingMatches.Count > 1)
                                            {
                                                (frameworkElement.FindName("gsBets1Main1") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center10);
                                                (frameworkElement.FindName("gsBets1Main2") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center10);
                                                (frameworkElement.FindName("gsBets2Main1") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center11);
                                                (frameworkElement.FindName("gsBets2Main1") as GradientStop).BeginAnimation(GradientStop.OffsetProperty, center11);
                                                (frameworkElement.FindName("stBettingStamp") as ScaleTransform).BeginAnimation(ScaleTransform.ScaleXProperty, center9);
                                                (frameworkElement.FindName("stBettingStamp") as ScaleTransform).BeginAnimation(ScaleTransform.ScaleYProperty, center9);
                                                (frameworkElement.FindName("gBettingStamp") as Grid).BeginAnimation(Grid.VisibilityProperty, center12);
                                                (frameworkElement.FindName("gBetsRatio") as Grid).BeginAnimation(Grid.VisibilityProperty, center13);

                                            }
                                            else
                                            {
                                                (frameworkElement.FindName("gBettingStamp") as Grid).BeginAnimation(Grid.VisibilityProperty, center15);
                                                (frameworkElement.FindName("gBetsRatio") as Grid).BeginAnimation(Grid.VisibilityProperty, center15);
                                            }
                                            (frameworkElement.FindName("gsBets1Main1") as GradientStop).Offset = bM.odds1 ?? 0;
                                            (frameworkElement.FindName("gsBets1Main2") as GradientStop).Offset = bM.odds1 ?? 0;

                                            (frameworkElement.FindName("gsBets2Main1") as GradientStop).Offset = bM.odds2 ?? 0;
                                            (frameworkElement.FindName("gsBets2Main2") as GradientStop).Offset = bM.odds2 ?? 0;


                                        }
                                        else
                                        {
                                            (frameworkElement.FindName("gBettingStamp") as Grid).BeginAnimation(Grid.VisibilityProperty, center14, HandoffBehavior.Compose);
                                            (frameworkElement.FindName("gBetsRatio") as Grid).BeginAnimation(Grid.VisibilityProperty, center14, HandoffBehavior.Compose);

                                        }
                                    }
                                    else
                                    {
                                        (frameworkElement.FindName("gBettingStamp") as Grid).BeginAnimation(Grid.VisibilityProperty, center14, HandoffBehavior.Compose);
                                        (frameworkElement.FindName("gBetsRatio") as Grid).BeginAnimation(Grid.VisibilityProperty, center14, HandoffBehavior.Compose);

                                    }


                                    (frameworkElement.FindName("tbMatchTime") as TextBlock).Text = DM.time;
                                    (frameworkElement.FindName("tbCasters") as TextBlock).Text = DM.casters;
                                    if (!DM.isInterview)
                                    {
                                        (frameworkElement.FindName("iInterview") as Image).Visibility = Visibility.Hidden;
                                        (frameworkElement.FindName("tbInterviewed") as TextBlock).Text = "";
                                        (frameworkElement.FindName("tbName1") as TextBlock).Text = DM.p1;
                                        (frameworkElement.FindName("tbName2") as TextBlock).Text = DM.p2;

                                        if (displayingMatches.Count > 1)
                                        {
                                            (frameworkElement.FindName("tbMatchNumber") as TextBlock).Text = "Match #" + DMIndex.ToString();
                                        }
                                        DMIndex++;
                                        (frameworkElement.FindName("tbRank1") as TextBlock).Text = FormatPR(DM.rank1);
                                        (frameworkElement.FindName("tbRank2") as TextBlock).Text = FormatPR(DM.rank2);
                                        (frameworkElement.FindName("tbRoundTitle1") as TextBlock).Text = DM.round;
                                        (frameworkElement.FindName("tbBestOfTitle1") as TextBlock).Text = DM.BO;


                                        try
                                        {
                                            (frameworkElement.FindName("iFlag1") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/" + DM.f1));
                                        }
                                        catch
                                        {
                                            (frameworkElement.FindName("iFlag1") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/xy.png"));
                                        }

                                        try
                                        {
                                            (frameworkElement.FindName("iFlag2") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/" + DM.f2));
                                        }
                                        catch
                                        {
                                            (frameworkElement.FindName("iFlag2") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/xy.png"));
                                        }

                                        try
                                        {
                                            (frameworkElement.FindName("iAvatar1") as Image).Source = new BitmapImage(new Uri("pack://application:,,,/Avatars/" + DM.a1));
                                        }
                                        catch
                                        {
                                            (frameworkElement.FindName("iAvatar1") as Image).Source = new BitmapImage(new Uri("pack://application:,,,/Avatars/unknown.png"));
                                        }
                                        try
                                        {
                                            (frameworkElement.FindName("iAvatar2") as Image).Source = new BitmapImage(new Uri("pack://application:,,,/Avatars/" + DM.a2));
                                        }
                                        catch
                                        {
                                            (frameworkElement.FindName("iAvatar2") as Image).Source = new BitmapImage(new Uri("pack://application:,,,/Avatars/unknown.png"));
                                        }
                                        (frameworkElement.FindName("iAvatar1") as Image).Visibility = Visibility.Visible;
                                        (frameworkElement.FindName("iAvatar2") as Image).Visibility = Visibility.Visible;
                                        (frameworkElement.FindName("bFlag1") as Border).Visibility = Visibility.Visible;
                                        (frameworkElement.FindName("bFlag2") as Border).Visibility = Visibility.Visible;
                                        (frameworkElement.FindName("tbVS1") as TextBlock).Visibility = Visibility.Visible;

                                        (frameworkElement.FindName("bAvatar1") as Border).Visibility = Visibility.Visible;
                                        (frameworkElement.FindName("bAvatar2") as Border).Visibility = Visibility.Visible;
                                        if (displayingMatches.Count > 1)
                                        {
                                            (frameworkElement.FindName("ttGridMatch") as TranslateTransform).BeginAnimation(TranslateTransform.YProperty, center);
                                            (frameworkElement.FindName("gDisplayMatch") as Grid).BeginAnimation(Grid.OpacityProperty, center2);
                                            (frameworkElement.FindName("ttGridMatch") as TranslateTransform).BeginAnimation(TranslateTransform.YProperty, center3, HandoffBehavior.Compose);
                                            (frameworkElement.FindName("gDisplayMatch") as Grid).BeginAnimation(Grid.OpacityProperty, center4, HandoffBehavior.Compose);

                                            await Task.Delay(1500);


                                        }
                                    }
                                    else
                                    {
                                        (frameworkElement.FindName("iInterview") as Image).Visibility = Visibility.Visible;
                                        (frameworkElement.FindName("tbInterviewed") as TextBlock).Text = DM.InterviewTitle;
                                        (frameworkElement.FindName("iInterview") as Image).Source = new BitmapImage(new Uri("pack://application:,,,/Elements/Interview.png"));
                                        (frameworkElement.FindName("tbName1") as TextBlock).Text = "";
                                        (frameworkElement.FindName("tbName2") as TextBlock).Text = "";
                                        (frameworkElement.FindName("tbMatchNumber") as TextBlock).Text = "";
                                        (frameworkElement.FindName("tbRank1") as TextBlock).Text = "";
                                        (frameworkElement.FindName("tbRank2") as TextBlock).Text = "";
                                        (frameworkElement.FindName("tbRoundTitle1") as TextBlock).Text = "";
                                        (frameworkElement.FindName("tbBestOfTitle1") as TextBlock).Text = "";
                                        (frameworkElement.FindName("tbVS1") as TextBlock).Visibility = Visibility.Hidden;
                                        (frameworkElement.FindName("iAvatar1") as Image).Visibility = Visibility.Hidden;
                                        (frameworkElement.FindName("iAvatar2") as Image).Visibility = Visibility.Hidden;
                                        (frameworkElement.FindName("bFlag1") as Border).Visibility = Visibility.Hidden;
                                        (frameworkElement.FindName("bFlag2") as Border).Visibility = Visibility.Hidden;
                                        (frameworkElement.FindName("bAvatar1") as Border).Visibility = Visibility.Hidden;
                                        (frameworkElement.FindName("bAvatar2") as Border).Visibility = Visibility.Hidden;
                                        (frameworkElement.FindName("ttGridMatch") as TranslateTransform).BeginAnimation(TranslateTransform.YProperty, center);
                                        (frameworkElement.FindName("gDisplayMatch") as Grid).BeginAnimation(Grid.OpacityProperty, center2);
                                        await Task.Delay(9000);
                                        (frameworkElement.FindName("ttGridMatch") as TranslateTransform).BeginAnimation(TranslateTransform.YProperty, center3, HandoffBehavior.Compose);
                                        (frameworkElement.FindName("gDisplayMatch") as Grid).BeginAnimation(Grid.OpacityProperty, center4, HandoffBehavior.Compose);

                                        await Task.Delay(1500);




                                    }


                                    if (frameworkElement.FindName("gMapPool") != null)
                                    {
                                        (frameworkElement.FindName("gMapPool") as Grid).Visibility = Visibility.Visible;
                                    }
                                    if (frameworkElement.FindName("gMapPool") != null)
                                    {
                                        if (DM.maps.Count > 0)
                                        {
                                            DoubleAnimation center5 = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(0.8));
                                            CircleEase ease5 = new CircleEase();
                                            ease5.EasingMode = EasingMode.EaseInOut;
                                            center5.EasingFunction = ease5;


                                            DoubleAnimation center7 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(1));
                                            center7.EasingFunction = ease5;
                                            center7.BeginTime = TimeSpan.FromSeconds(5);
                                            int MapIndex = 1;
                                            foreach (KeyValuePair<string, string> map in DM.maps)
                                            {

                                                (frameworkElement.FindName("tbMapPool") as TextBlock).Text = "#" + MapIndex.ToString() + " " + map.Key.Replace("ESOC ", "");
                                                (frameworkElement.FindName("iMapPool") as Image).Source = new BitmapImage(new Uri("pack://application:,,,/Maps/" + map.Value));

                                                (frameworkElement.FindName("stMapPool") as ScaleTransform).BeginAnimation(ScaleTransform.ScaleXProperty, center5);
                                                (frameworkElement.FindName("stMapPool") as ScaleTransform).BeginAnimation(ScaleTransform.ScaleYProperty, center5);
                                                (frameworkElement.FindName("gMapPool") as Grid).BeginAnimation(Grid.OpacityProperty, center5);

                                                (frameworkElement.FindName("stMapPool") as ScaleTransform).BeginAnimation(ScaleTransform.ScaleXProperty, center7, HandoffBehavior.Compose);
                                                (frameworkElement.FindName("stMapPool") as ScaleTransform).BeginAnimation(ScaleTransform.ScaleYProperty, center7, HandoffBehavior.Compose);
                                                (frameworkElement.FindName("gMapPool") as Grid).BeginAnimation(Grid.OpacityProperty, center7, HandoffBehavior.Compose);
                                                MapIndex++;
                                                await Task.Delay(6000);
                                            }
                                        }
                                        else
                                            if (!DM.isInterview)
                                            await Task.Delay(5000);

                                    }
                                    else
                                        if (!DM.isInterview)
                                        await Task.Delay(5000);


                                    if (frameworkElement.FindName("gMapPool") != null)
                                    {
                                        (frameworkElement.FindName("gMapPool") as Grid).Visibility = Visibility.Hidden;
                                    }


                                    if (displayingMatches.Count > 1)
                                    {
                                        (frameworkElement.FindName("gBettingStamp") as Grid).BeginAnimation(Grid.VisibilityProperty, center14, HandoffBehavior.Compose);
                                        (frameworkElement.FindName("gBetsRatio") as Grid).BeginAnimation(Grid.VisibilityProperty, center14, HandoffBehavior.Compose);
                                        await Task.Delay(2000);

                                    }

                                }


                                mainWindow.AnimateMatch.Start();
                            }
                            else
                            {
                                (frameworkElement.FindName("tbName1") as TextBlock).Visibility = Visibility.Collapsed;
                                (frameworkElement.FindName("tbName2") as TextBlock).Visibility = Visibility.Collapsed;

                                (frameworkElement.FindName("tbRoundTitle1") as TextBlock).Visibility = Visibility.Collapsed;
                                (frameworkElement.FindName("tbBestOfTitle1") as TextBlock).Visibility = Visibility.Collapsed;
                                (frameworkElement.FindName("tbVS1") as TextBlock).Visibility = Visibility.Collapsed;

                                (frameworkElement.FindName("iAvatar1") as Image).Visibility = Visibility.Collapsed;
                                (frameworkElement.FindName("iAvatar2") as Image).Visibility = Visibility.Collapsed;

                                (frameworkElement.FindName("bAvatar1") as Border).Visibility = Visibility.Collapsed;
                                (frameworkElement.FindName("bAvatar2") as Border).Visibility = Visibility.Collapsed;
                            }
                        };
                        mainWindow.AnimateMatch.Start();
                    }
                }
                if (Name.type == "mu")
                {
                    (frameworkElement.FindName("tbName1") as TextBlock).Text = (Application.Current.MainWindow as SettingsDialog).tbPlayer1.Text;
                    (frameworkElement.FindName("tbName2") as TextBlock).Text = (Application.Current.MainWindow as SettingsDialog).tbPlayer2.Text;


                    (frameworkElement.FindName("tbRank1") as TextBlock).Text = ((KeyValuePair<string, string>)(Application.Current.MainWindow as SettingsDialog).tbPlayer1.Tag).Key;
                    (frameworkElement.FindName("tbRank2") as TextBlock).Text = ((KeyValuePair<string, string>)(Application.Current.MainWindow as SettingsDialog).tbPlayer2.Tag).Key;

                    (frameworkElement.FindName("iFlag1") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Civ/" + GetCivByIndex((Application.Current.MainWindow as SettingsDialog).civSelector1.SelectedIndex)));

                    (frameworkElement.FindName("iFlag2") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Civ/" + GetCivByIndex((Application.Current.MainWindow as SettingsDialog).civSelector2.SelectedIndex)));

                    (frameworkElement.FindName("iAvatar1") as Image).Source = new BitmapImage(new Uri(((KeyValuePair<string, string>)(Application.Current.MainWindow as SettingsDialog).tbPlayer1.Tag).Value));

                    (frameworkElement.FindName("iAvatar2") as Image).Source = new BitmapImage(new Uri(((KeyValuePair<string, string>)(Application.Current.MainWindow as SettingsDialog).tbPlayer2.Tag).Value));
                    (frameworkElement.FindName("bAvatar1") as Border).Visibility = Visibility.Visible;
                    (frameworkElement.FindName("bAvatar2") as Border).Visibility = Visibility.Visible;



                    if (frameworkElement.FindName("gMapPool") != null)
                    {
                        List<Map> maps = new List<Map>((Application.Current.MainWindow as SettingsDialog).SelectedMaps.maps);
                        if (maps.Count > 0)
                        {
                            (frameworkElement.FindName("iMapFrame") as Image).Source = new BitmapImage(new Uri("pack://application:,,,/MapUI/" + Name.map_frame));
                            (frameworkElement.FindName("ibMapTitle") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/MapUI/" + Name.map_title));

                            DoubleAnimation center = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(0.8));
                            CircleEase ease1 = new CircleEase();
                            ease1.EasingMode = EasingMode.EaseInOut;
                            center.EasingFunction = ease1;

                            DoubleAnimation center2 = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(1));
                            center2.EasingFunction = ease1;
                            DoubleAnimation center3 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(1));
                            center3.EasingFunction = ease1;
                            center3.BeginTime = TimeSpan.FromSeconds(6);
                            DoubleAnimation center4 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(0.8));
                            center4.EasingFunction = ease1;
                            center4.BeginTime = TimeSpan.FromSeconds(6);


                            DoubleAnimation center5 = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(0.8));
                            CircleEase ease5 = new CircleEase();
                            ease5.EasingMode = EasingMode.EaseInOut;
                            center5.EasingFunction = ease5;


                            DoubleAnimation center7 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(1));
                            center7.EasingFunction = ease5;
                            center7.BeginTime = TimeSpan.FromSeconds(6);
                            mainWindow.AnimateMatch.Interval = new TimeSpan(0, 0, 0);

                            TextBlock tbMapPool = (frameworkElement.FindName("tbMapPool") as TextBlock);
                            Image iMapPool = (frameworkElement.FindName("iMapPool") as Image);
                            ScaleTransform stMap = (frameworkElement.FindName("stMap") as ScaleTransform);
                            GradientStop MapTransparentStop = (frameworkElement.FindName("MapTransparentStop") as GradientStop);
                            GradientStop MapBlackStop = (frameworkElement.FindName("MapBlackStop") as GradientStop);
                            Grid gMapWithFrame = (frameworkElement.FindName("gMapWithFrame") as Grid);

                            mainWindow.AnimateMatch.Tick += async (object s, EventArgs eventArgs) =>
                            {
                                mainWindow.AnimateMatch.Stop();



                                int MapIndex = 1;
                                foreach (Map map in maps)
                                {

                                    tbMapPool.Text = "#" + MapIndex.ToString() + " " + map.title;
                                    iMapPool.Source = new BitmapImage(new Uri("pack://application:,,,/Maps/" + map.map_name));

                                    stMap.BeginAnimation(ScaleTransform.ScaleXProperty, center5);
                                    stMap.BeginAnimation(ScaleTransform.ScaleYProperty, center5);

                                    MapTransparentStop.BeginAnimation(GradientStop.OffsetProperty, center);
                                    MapBlackStop.BeginAnimation(GradientStop.OffsetProperty, center2);

                                    gMapWithFrame.BeginAnimation(Grid.OpacityProperty, center5);

                                    if (maps.Count > 1)
                                    {
                                        stMap.BeginAnimation(ScaleTransform.ScaleXProperty, center7, HandoffBehavior.Compose);
                                        stMap.BeginAnimation(ScaleTransform.ScaleYProperty, center7, HandoffBehavior.Compose);


                                        gMapWithFrame.BeginAnimation(Grid.OpacityProperty, center7, HandoffBehavior.Compose);
                                        MapTransparentStop.BeginAnimation(GradientStop.OffsetProperty, center3, HandoffBehavior.Compose);
                                        MapBlackStop.BeginAnimation(GradientStop.OffsetProperty, center4, HandoffBehavior.Compose);
                                    }

                                    MapIndex++;
                                    await Task.Delay(7000);
                                }
                                if (maps.Count > 1)
                                    mainWindow.AnimateMatch.Start();
                            };
                            mainWindow.AnimateMatch.Start();

                        }

                    }


                }
                if (Name.type == "manual")
                {
                    (frameworkElement.FindName("tbRoundTitle1") as TextBlock).Text = (Application.Current.MainWindow as SettingsDialog).ManualRoundList.Text;

                    (frameworkElement.FindName("tbName1") as TextBlock).Text = (Application.Current.MainWindow as SettingsDialog).tbManualPlayer1.Text;
                    (frameworkElement.FindName("tbName2") as TextBlock).Text = (Application.Current.MainWindow as SettingsDialog).tbManualPlayer2.Text;

                    if ((Application.Current.MainWindow as SettingsDialog).ManualcivSelector1.SelectedIndex != 0)

                    (frameworkElement.FindName("iFlag1") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Civ/" + GetCivByIndex((Application.Current.MainWindow as SettingsDialog).ManualcivSelector1.SelectedIndex - 1)));
                    if ((Application.Current.MainWindow as SettingsDialog).ManualcivSelector2.SelectedIndex != 0)
                    (frameworkElement.FindName("iFlag2") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Civ/" + GetCivByIndex((Application.Current.MainWindow as SettingsDialog).ManualcivSelector2.SelectedIndex - 1)));


                    (frameworkElement.FindName("tbRank1") as TextBlock).Text = ((KeyValuePair<string, string>)(Application.Current.MainWindow as SettingsDialog).tbManualPlayer1.Tag).Key;
                    (frameworkElement.FindName("tbRank2") as TextBlock).Text = ((KeyValuePair<string, string>)(Application.Current.MainWindow as SettingsDialog).tbManualPlayer2.Tag).Key;

                    (frameworkElement.FindName("iAvatar1") as Image).Source = new BitmapImage(new Uri(((KeyValuePair<string, string>)(Application.Current.MainWindow as SettingsDialog).tbManualPlayer1.Tag).Value));

                    (frameworkElement.FindName("iAvatar2") as Image).Source = new BitmapImage(new Uri(((KeyValuePair<string, string>)(Application.Current.MainWindow as SettingsDialog).tbManualPlayer2.Tag).Value));
                    (frameworkElement.FindName("bAvatar1") as Border).Visibility = Visibility.Visible;
                    (frameworkElement.FindName("bAvatar2") as Border).Visibility = Visibility.Visible;



                    if (frameworkElement.FindName("gMapPool") != null)
                    {
                        List<Map> maps = new List<Map>((Application.Current.MainWindow as SettingsDialog).ManualSelectedMaps.maps);
                        if (maps.Count > 0)
                        {
                            (frameworkElement.FindName("iMapFrame") as Image).Source = new BitmapImage(new Uri("pack://application:,,,/MapUI/" + Name.map_frame));
                            (frameworkElement.FindName("ibMapTitle") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/MapUI/" + Name.map_title));

                            DoubleAnimation center = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(0.8));
                            CircleEase ease1 = new CircleEase();
                            ease1.EasingMode = EasingMode.EaseInOut;
                            center.EasingFunction = ease1;

                            DoubleAnimation center2 = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(1));
                            center2.EasingFunction = ease1;
                            DoubleAnimation center3 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(1));
                            center3.EasingFunction = ease1;
                            center3.BeginTime = TimeSpan.FromSeconds(6);
                            DoubleAnimation center4 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(0.8));
                            center4.EasingFunction = ease1;
                            center4.BeginTime = TimeSpan.FromSeconds(6);


                            DoubleAnimation center5 = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(0.8));
                            CircleEase ease5 = new CircleEase();
                            ease5.EasingMode = EasingMode.EaseInOut;
                            center5.EasingFunction = ease5;


                            DoubleAnimation center7 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(1));
                            center7.EasingFunction = ease5;
                            center7.BeginTime = TimeSpan.FromSeconds(6);
                            mainWindow.AnimateMatch.Interval = new TimeSpan(0, 0, 0);

                            TextBlock tbMapPool = (frameworkElement.FindName("tbMapPool") as TextBlock);
                            Image iMapPool = (frameworkElement.FindName("iMapPool") as Image);
                            ScaleTransform stMap = (frameworkElement.FindName("stMap") as ScaleTransform);
                            GradientStop MapTransparentStop = (frameworkElement.FindName("MapTransparentStop") as GradientStop);
                            GradientStop MapBlackStop = (frameworkElement.FindName("MapBlackStop") as GradientStop);
                            Grid gMapWithFrame = (frameworkElement.FindName("gMapWithFrame") as Grid);

                            mainWindow.AnimateMatch.Tick += async (object s, EventArgs eventArgs) =>
                            {
                                mainWindow.AnimateMatch.Stop();



                                int MapIndex = 1;
                                foreach (Map map in maps)
                                {

                                    tbMapPool.Text = "#" + MapIndex.ToString() + " " + map.title;
                                    iMapPool.Source = new BitmapImage(new Uri("pack://application:,,,/Maps/" + map.map_name));

                                    stMap.BeginAnimation(ScaleTransform.ScaleXProperty, center5);
                                    stMap.BeginAnimation(ScaleTransform.ScaleYProperty, center5);

                                    MapTransparentStop.BeginAnimation(GradientStop.OffsetProperty, center);
                                    MapBlackStop.BeginAnimation(GradientStop.OffsetProperty, center2);

                                    gMapWithFrame.BeginAnimation(Grid.OpacityProperty, center5);

                                    if (maps.Count > 1)
                                    {
                                        stMap.BeginAnimation(ScaleTransform.ScaleXProperty, center7, HandoffBehavior.Compose);
                                        stMap.BeginAnimation(ScaleTransform.ScaleYProperty, center7, HandoffBehavior.Compose);


                                        gMapWithFrame.BeginAnimation(Grid.OpacityProperty, center7, HandoffBehavior.Compose);
                                        MapTransparentStop.BeginAnimation(GradientStop.OffsetProperty, center3, HandoffBehavior.Compose);
                                        MapBlackStop.BeginAnimation(GradientStop.OffsetProperty, center4, HandoffBehavior.Compose);
                                    }

                                    MapIndex++;
                                    await Task.Delay(7000);
                                }
                                if (maps.Count > 1)
                                    mainWindow.AnimateMatch.Start();
                            };
                            mainWindow.AnimateMatch.Start();

                        }

                    }


                }
                if (Name.type == "wt")
                {
                    (frameworkElement.FindName("tbRoundTitle1") as TextBlock).Text = (Application.Current.MainWindow as SettingsDialog).WTRoundList.Text;
                    (frameworkElement.FindName("tbName1") as TextBlock).Text = (Application.Current.MainWindow as SettingsDialog).WTPlayer1.Text;
                    (frameworkElement.FindName("tbName2") as TextBlock).Text = (Application.Current.MainWindow as SettingsDialog).WTPlayer2.Text;

                    (frameworkElement.FindName("tbRank1") as TextBlock).Text = FormatPR(((((Application.Current.MainWindow as SettingsDialog).WTPlayer1.SelectedItem as ComboBoxItem).Tag as Brackets.Player)).power_rating);
                    (frameworkElement.FindName("tbRank2") as TextBlock).Text = FormatPR(((((Application.Current.MainWindow as SettingsDialog).WTPlayer2.SelectedItem as ComboBoxItem).Tag as Brackets.Player)).power_rating);


                    try
                    {
                        (frameworkElement.FindName("iFlag1") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/" + ((Brackets.Player)((Application.Current.MainWindow as SettingsDialog).WTPlayer1.SelectedItem as ComboBoxItem).Tag).flag_image));
                    }
                    catch
                    {
                        (frameworkElement.FindName("iFlag1") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/xy.png"));
                    }

                    try
                    {
                        (frameworkElement.FindName("iFlag2") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/" + ((Brackets.Player)((Application.Current.MainWindow as SettingsDialog).WTPlayer2.SelectedItem as ComboBoxItem).Tag).flag_image));
                    }
                    catch
                    {
                        (frameworkElement.FindName("iFlag2") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/Flags/xy.png"));
                    }


                    try
                    {
                        (frameworkElement.FindName("iAvatar1") as Image).Source = new BitmapImage(new Uri("pack://application:,,,/Avatars/" + ((Brackets.Player)((Application.Current.MainWindow as SettingsDialog).WTPlayer1.SelectedItem as ComboBoxItem).Tag).avatar_image));
                    }
                    catch
                    {
                        (frameworkElement.FindName("iAvatar1") as Image).Source = new BitmapImage(new Uri("pack://application:,,,/Avatars/unknown.png"));
                    }

                    try
                    {
                        (frameworkElement.FindName("iAvatar2") as Image).Source = new BitmapImage(new Uri("pack://application:,,,/Avatars/" + ((Brackets.Player)((Application.Current.MainWindow as SettingsDialog).WTPlayer2.SelectedItem as ComboBoxItem).Tag).avatar_image));
                    }
                    catch
                    {
                        (frameworkElement.FindName("iAvatar2") as Image).Source = new BitmapImage(new Uri("pack://application:,,,/Avatars/unknown.png"));
                    }

                    (frameworkElement.FindName("bAvatar1") as Border).Visibility = Visibility.Visible;
                    (frameworkElement.FindName("bAvatar2") as Border).Visibility = Visibility.Visible;



                    if (frameworkElement.FindName("gMapPool") != null)
                    {
                        
                        Dictionary<string, string> maps = new Dictionary<string, string>();
                        foreach (Brackets.MapPool map in (((Application.Current.MainWindow as SettingsDialog).WTRoundList.SelectedItem as ComboBoxItem).Tag as Brackets.Round).map_pool)
                        {
                                maps.Add((Application.Current.MainWindow as SettingsDialog).WTcurrentBrackets.maps[map.order].name,
                                 (Application.Current.MainWindow as SettingsDialog).WTcurrentBrackets.maps[map.order].minimap_image.Replace("esoc_", ""));
                        }

                        if (maps.Count > 0)
                        {
                            (frameworkElement.FindName("iMapFrame") as Image).Source = new BitmapImage(new Uri("pack://application:,,,/MapUI/" + Name.map_frame));
                            (frameworkElement.FindName("ibMapTitle") as ImageBrush).ImageSource = new BitmapImage(new Uri("pack://application:,,,/MapUI/" + Name.map_title));

                            DoubleAnimation center = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(0.8));
                            CircleEase ease1 = new CircleEase();
                            ease1.EasingMode = EasingMode.EaseInOut;
                            center.EasingFunction = ease1;

                            DoubleAnimation center2 = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(1));
                            center2.EasingFunction = ease1;
                            DoubleAnimation center3 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(1));
                            center3.EasingFunction = ease1;
                            center3.BeginTime = TimeSpan.FromSeconds(6);
                            DoubleAnimation center4 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(0.8));
                            center4.EasingFunction = ease1;
                            center4.BeginTime = TimeSpan.FromSeconds(6);


                            DoubleAnimation center5 = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(0.8));
                            CircleEase ease5 = new CircleEase();
                            ease5.EasingMode = EasingMode.EaseInOut;
                            center5.EasingFunction = ease5;


                            DoubleAnimation center7 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(1));
                            center7.EasingFunction = ease5;
                            center7.BeginTime = TimeSpan.FromSeconds(6);
                            mainWindow.AnimateMatch.Interval = new TimeSpan(0, 0, 0);

                            TextBlock tbMapPool = (frameworkElement.FindName("tbMapPool") as TextBlock);
                            Image iMapPool = (frameworkElement.FindName("iMapPool") as Image);
                            ScaleTransform stMap = (frameworkElement.FindName("stMap") as ScaleTransform);
                            GradientStop MapTransparentStop = (frameworkElement.FindName("MapTransparentStop") as GradientStop);
                            GradientStop MapBlackStop = (frameworkElement.FindName("MapBlackStop") as GradientStop);
                            Grid gMapWithFrame = (frameworkElement.FindName("gMapWithFrame") as Grid);

                            mainWindow.AnimateMatch.Tick += async (object s, EventArgs eventArgs) =>
                            {
                                mainWindow.AnimateMatch.Stop();



                                int MapIndex = 1;
                                foreach (KeyValuePair<string,string> map in maps)
                                {

                                    tbMapPool.Text = "#" + MapIndex.ToString() + " " + map.Key.Replace("ESOC ", "");
                                    iMapPool.Source = new BitmapImage(new Uri("pack://application:,,,/Maps/" + map.Value));

                                    stMap.BeginAnimation(ScaleTransform.ScaleXProperty, center5);
                                    stMap.BeginAnimation(ScaleTransform.ScaleYProperty, center5);

                                    MapTransparentStop.BeginAnimation(GradientStop.OffsetProperty, center);
                                    MapBlackStop.BeginAnimation(GradientStop.OffsetProperty, center2);

                                    gMapWithFrame.BeginAnimation(Grid.OpacityProperty, center5);

                                    if (maps.Count > 1)
                                    {
                                        stMap.BeginAnimation(ScaleTransform.ScaleXProperty, center7, HandoffBehavior.Compose);
                                        stMap.BeginAnimation(ScaleTransform.ScaleYProperty, center7, HandoffBehavior.Compose);


                                        gMapWithFrame.BeginAnimation(Grid.OpacityProperty, center7, HandoffBehavior.Compose);
                                        MapTransparentStop.BeginAnimation(GradientStop.OffsetProperty, center3, HandoffBehavior.Compose);
                                        MapBlackStop.BeginAnimation(GradientStop.OffsetProperty, center4, HandoffBehavior.Compose);
                                    }

                                    MapIndex++;
                                    await Task.Delay(7000);
                                }
                                if (maps.Count > 1)
                                    mainWindow.AnimateMatch.Start();
                            };
                            mainWindow.AnimateMatch.Start();

                        }

                    }


                }
                mainWindow.InputBindings.Add(new InputBinding(mainWindow.TimeUp, new KeyGesture(Key.Up, ModifierKeys.Control)));
                mainWindow.InputBindings.Add(new InputBinding(mainWindow.TimeDown, new KeyGesture(Key.Down, ModifierKeys.Control)));
                mainWindow.InputBindings.Add(new InputBinding(mainWindow.CloseOverlay, new KeyGesture(Key.Escape)));
                mainWindow.InputBindings.Add(new InputBinding(mainWindow.MinimizeOverlay, new KeyGesture(Key.Space, ModifierKeys.Control)));

                frameworkElement.EndInit();
                frameworkElement.UpdateLayout();
            }
            mainWindow.Show();
            Application.Current.MainWindow.Hide();
            this.Hide();
        }


        public string GetCivByIndex(int index)
        {
            switch (index)
            {
                case 0: return "British.png";
                case 1: return "Dutch.png";
                case 2: return "French.png";
                case 3: return "Germans.png";
                case 4: return "Ottomans.png";
                case 5: return "Portuguese.png";
                case 6: return "Russians.png";
                case 7: return "Spanish.png";
                case 8: return "XPIroquois.png";
                case 9: return "XPSioux.png";
                case 10: return "XPAztec.png";
                case 11: return "Chinese.png";
                case 12: return "Indians.png";
                case 13: return "Japanese.png";
            }
            return "";
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string[] Scopes = { SheetsService.Scope.SpreadsheetsReadonly };
            string ApplicationName = "Stream Overlay";
            string Name = (((Application.Current.MainWindow as SettingsDialog).OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).bettings;
            if (!string.IsNullOrEmpty(Name))
            {
                try
                {
                    UserCredential credential;

                    using (var stream =
                        new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
                    {
                        string credPath = "token.json";
                        credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                            GoogleClientSecrets.Load(stream).Secrets,
                            Scopes,
                            "user",
                            CancellationToken.None,
                            new FileDataStore(credPath, true)).Result;
                    }

                    // Create Google Sheets API service.
                    var service = new SheetsService(new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = ApplicationName,
                    });

                    // Define request parameters.
                    String spreadsheetId = Name;
                    String range = "Standings!I5:M13";
                    SpreadsheetsResource.ValuesResource.GetRequest request =
                            service.Spreadsheets.Values.Get(spreadsheetId, range);

                    Google.Apis.Sheets.v4.Data.ValueRange response = await request.ExecuteAsync();
                    IList<IList<Object>> values = response.Values;
                    if (values != null && values.Count > 0)
                    {
                        for (int i = 0; i < values[2].Count; i++)
                            if (values[2][i].ToString() == "OPEN")
                            {
                                double? o1;
                                double? o2;

                                if (values[7][i] == null)
                                {
                                    o1 = null;
                                }
                                else
                                {
                                    o1 = Convert.ToDouble(values[7][i], CultureInfo.InvariantCulture);
                                }

                                if (values[8][i] == null)
                                {
                                    o2 = null;
                                }
                                else
                                {
                                    o2 = Convert.ToDouble(values[8][i], CultureInfo.InvariantCulture);
                                }


                                if (o1 != null && o2 != null && (o1 + o2) != 0)
                                {
                                    o1 = o2 / (o1 + o2);
                                    o2 = 1 - o1;
                                }
                                bettingMatches.Add(new BettingMatch() { p1 = values[0][i].ToString(), p2 = values[1][i].ToString(), odds1 = o1, odds2 = o2 });
                            }
                    }
                }
                catch { }
            }
        }

    }

}
